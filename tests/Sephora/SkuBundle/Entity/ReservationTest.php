<?php
namespace Tests\Sephora\SkuBundle\Entity;

use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Customer;
use Sephora\SkuBundle\Entity\Inventory;
use Sephora\SkuBundle\Entity\Reservation;

/**
 * Class ReservationTest
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Entity
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class ReservationTest extends TestCase
{
    public function testNew()
    {
        $entity = new Reservation();
        $entity->generateId();

        $this->assertEquals(1, preg_match('/^[a-z0-9]+(-[a-z0-9]+){4}$/', $entity->getId()));
        $this->assertEquals(36, strlen($entity->getId()));

        $entity->setNoItems(3);
        $this->assertEquals(3, $entity->getNoItems());

        $entity->setCustomer(new Customer());
        $this->assertTrue($entity->getCustomer() instanceof Customer);

        $entity->setInventory(new Inventory());
        $this->assertTrue($entity->getInventory() instanceof Inventory);
    }

    /**
     * Tests if generateId() respects current value of id
     */
    public function testIdAlreadySet()
    {
        $entity = new Reservation();
        try {
            $reflection = new \ReflectionClass($entity);
        } catch (\ReflectionException $ex) {
            $this->assertTrue(false, 'Reflection exception');
        }
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($entity, 'test-id');

        $entity->generateId();

        $this->assertEquals('test-id', $entity->getId());
    }
}
