<?php
namespace Tests\Sephora\SkuBundle\Entity;

use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Warehouse;

/**
 * Class Warehouse
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Entity
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class WarehouseTest extends TestCase
{
    /**
     * Tests generateId() and metadata getters/setters
     */
    public function testNew()
    {
        $entity = new Warehouse();
        $entity->generateId();
        $name = 'test name';
        $entity->setName($name);

        $this->assertEquals($name, $entity->getName());
        $this->assertEquals(1, preg_match('/^[a-z0-9]+(-[a-z0-9]+){4}$/', $entity->getId()));
        $this->assertEquals(36, strlen($entity->getId()));
    }

    /**
     * Tests if generateId() respects current value of id
     */
    public function testIdAlreadySet()
    {
        $entity = new Warehouse();
        try {
            $reflection = new \ReflectionClass($entity);
        } catch (\ReflectionException $ex) {
            $this->assertTrue(false, 'Reflection exception');
        }
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($entity, 'test-id');

        $entity->generateId();

        $this->assertEquals('test-id', $entity->getId());
    }
}
