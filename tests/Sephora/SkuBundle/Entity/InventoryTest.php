<?php
namespace Tests\Sephora\SkuBundle\Entity;

use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Inventory;
use Sephora\SkuBundle\Entity\Product;
use Sephora\SkuBundle\Entity\Warehouse;

/**
 * Class InventoryTest
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Entity
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class InventoryTest extends TestCase
{
    public function testNew()
    {
        $entity = new Inventory();
        $entity->generateId();

        $this->assertEquals(1, preg_match('/^[a-z0-9]+(-[a-z0-9]+){4}$/', $entity->getId()));
        $this->assertEquals(36, strlen($entity->getId()));

        $entity->setNoStock(5);
        $this->assertEquals(5, $entity->getNoStock());
        $entity->addNoStock(2);
        $this->assertEquals(7, $entity->getNoStock());
        $entity->reduceNoStock(4);
        $this->assertEquals(3, $entity->getNoStock());

        $entity->setNoReserved(3);
        $this->assertEquals(3, $entity->getNoReserved());
        $entity->addNoReserved(7);
        $this->assertEquals(10, $entity->getNoReserved());
        $entity->reduceNoReserved(2);
        $this->assertEquals(8, $entity->getNoReserved());

        $this->assertEquals(0, $entity->getVersion());

        $entity->setProduct(new Product());
        $this->assertTrue($entity->getProduct() instanceof Product);

        $entity->setWarehouse(new Warehouse());
        $this->assertTrue($entity->getWarehouse() instanceof Warehouse);
    }

    /**
     * Tests if generateId() respects current value of id
     */
    public function testIdAlreadySet()
    {
        $entity = new Inventory();
        try {
            $reflection = new \ReflectionClass($entity);
        } catch (\ReflectionException $ex) {
            $this->assertTrue(false, 'Reflection exception');
        }
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($entity, 'test-id');

        $entity->generateId();

        $this->assertEquals('test-id', $entity->getId());
    }
}
