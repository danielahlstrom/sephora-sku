<?php
namespace Tests\Sephora\SkuBundle\Entity;

use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Customer;
use Sephora\SkuBundle\Entity\Reservation;

/**
 * Class CustomerTest
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Entity
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class CustomerTest extends TestCase
{
    public function testNewCustomer()
    {
        $entity = new Customer();
        $entity->generateId();
        $name = 'test name';
        $entity->setName($name);

        $this->assertEquals($name, $entity->getName());
        $this->assertEquals(1, preg_match('/^[a-z0-9]+(-[a-z0-9]+){4}$/', $entity->getId()));
        $this->assertEquals(36, strlen($entity->getId()));

        $this->assertEmpty($entity->getReservations());

        $entity->addReservation(new Reservation());
        $this->assertCount(1, $entity->getReservations());
        foreach ($entity->getReservations() as $reservation) {
            $this->assertTrue($reservation instanceof Reservation);
        }
    }

    /**
     * Tests if generateId() respects current value of id
     */
    public function testIdAlreadySet()
    {
        $entity = new Customer();
        try {
            $reflection = new \ReflectionClass($entity);
        } catch (\ReflectionException $ex) {
            $this->assertTrue(false, 'Reflection exception');

            return;
        }
        $idProperty = $reflection->getProperty('id');
        $idProperty->setAccessible(true);
        $idProperty->setValue($entity, 'test-id');

        $entity->generateId();

        $this->assertEquals('test-id', $entity->getId());
    }
}
