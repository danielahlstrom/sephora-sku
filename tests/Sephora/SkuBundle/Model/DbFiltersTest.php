<?php
namespace Tests\Sephora\SkuBundle\Model;

use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Model\DbFilters;

/**
 * Class DbFiltersTest
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Model
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class DbFiltersTest extends TestCase
{
    /**
     * Tests getters and setters
     */
    public function testNew()
    {
        $dbFilters = new DbFilters();

        $dbFilters->setCriteria(array('name' => 'test'));
        $dbFilters->setOrderBy(array('name' => 'ASC'));
        $dbFilters->setPage(2);
        $dbFilters->setPageSize(15);

        $this->assertEquals(array('name' => 'test'), $dbFilters->getCriteria());
        $this->assertEquals(array('name' => 'ASC'), $dbFilters->getOrderBy());
        $this->assertEquals(2, $dbFilters->getPage());
        $this->assertEquals(15, $dbFilters->getPageSize());
        $this->assertEquals(15, $dbFilters->getLimit());
        $this->assertEquals(2 * 15, $dbFilters->getOffset());
    }
}
