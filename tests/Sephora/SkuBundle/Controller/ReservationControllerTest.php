<?php
namespace Tests\Sephora\SkuBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class ReservationControllerTest
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Controller
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class ReservationControllerTest extends WebTestCase
{
    public function setUp()
    {
        $this->loadFixtures(array('Sephora\SkuBundle\DataFixtures\ORM\SkuTestFixtures'));
    }

    public function testList_500()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservations_list', array(
            '_format' => 'json',
            'page' => -1,
        ));

        $client->request('GET', $url);
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testList_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservations_list', array(
            '_format' => 'json',
            'page' => 0,
            'page_size' => 5,
        ));

        $client->request('GET', $url);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $this->assertCount(5, json_decode($responseContent, true));
    }

    public function testList_Csv_500()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservations_list_csv', array(
            '_format' => 'json',
            'page' => -1,
        ));

        $client->request('GET', $url);
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $this->assertNotFalse(strpos($client->getResponse()->headers->get('Content-Type'), 'text/plain'));
    }

    public function testList_Csv_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservations_list_csv', array(
            '_format' => 'json',
            'page' => 0,
            'page_size' => 5,
        ));

        $client->request('GET', $url);
        $this->isSuccessful($client->getResponse());
        $this->assertNotFalse(strpos($client->getResponse()->headers->get('Content-Type'), 'text/csv'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertEquals(5, substr_count($responseContent, PHP_EOL) + 1);
    }

    public function testSingle_404()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_get', array(
            'id' => 'invalid',
            '_format' => 'json',
        ));

        $client->request('GET', $url);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testSingle_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_get', array(
            'id' => 'luke-sing-pencil',
            '_format' => 'json',
        ));

        $client->request('GET', $url);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
    }

    public function testMakeReservation_InvalidJson_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_create', array(
            '_format' => 'json',
        ));

        $client->request('POST', $url, array(), array(), array(), '{malformed_json');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testMakeReservation_InvalidIds_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_create', array(
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'customer' => array(
                'id' => 'invalid',
            ),
            'inventory' => array(
                'id' => 'invalid',
            ),
            'no_items' => 1,
        ));

        $client->request('POST', $url, array(), array(), array(), $content);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testMakeReservation_FloatItems_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_create', array(
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'customer' => array(
                'id' => 'luke',
            ),
            'inventory' => array(
                'id' => 'sing-pencil',
            ),
            'no_items' => 0.5,
        ));

        $client->request('POST', $url, array(), array(), array(), $content);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testMakeReservation_NegativeItems_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_create', array(
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'customer' => array(
                'id' => 'luke',
            ),
            'inventory' => array(
                'id' => 'sing-pencil',
            ),
            'no_items' => -1,
        ));

        $client->request('POST', $url, array(), array(), array(), $content);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testMakeReservation_TooManyReserved_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_create', array(
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'customer' => array(
                'id' => 'luke',
            ),
            'inventory' => array(
                'id' => 'sing-pencil',
            ),
            'no_items' => 100,
        ));

        $client->request('POST', $url, array(), array(), array(), $content);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testMakeReservation_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_create', array(
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'customer' => array(
                'id' => 'luke',
            ),
            'inventory' => array(
                'id' => 'sing-pencil',
            ),
            'no_items' => 3,
        ));

        $client->request('POST', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $responseArray = json_decode($responseContent, true);
        $this->assertEquals(8, $responseArray['inventory']['no_reserved']);
    }

    public function testShipReservation_InvalidId_404()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_ship', array(
            'id' => 'invalid',
            '_format' => 'json',
        ));

        $client->request('PUT', $url);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testShipReservation_StockToLow_400()
    {
        // Set stock to 1
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'set',
            '_format' => 'json',
        ));
        $content = json_encode(array(
            'id' => 'sing-pencil',
            'no_stock' => 1,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $responseArray = json_decode($responseContent, true);
        $this->assertEquals(1, $responseArray['no_stock']);

        // Ship a reservation which has more items than stock
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_ship', array(
            'id' => 'luke-sing-pencil',
            '_format' => 'json',
        ));

        $client->request('PUT', $url);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testShipReservation_204()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('reservation_ship', array(
            'id' => 'luke-sing-pencil',
            '_format' => 'json',
        ));

        $client->request('PUT', $url);
        $this->assertEquals(204, $client->getResponse()->getStatusCode());
    }
}
