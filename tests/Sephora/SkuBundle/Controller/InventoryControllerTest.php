<?php
namespace Tests\Sephora\SkuBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class InventoryControllerTest
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Controller
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class InventoryControllerTest extends WebTestCase
{
    public function setUp()
    {
        $this->loadFixtures(array('Sephora\SkuBundle\DataFixtures\ORM\SkuTestFixtures'));
    }

    public function testSet_404()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'set',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'id' => 'invalid',
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testSet_NegativeStock_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'set',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'id' => 'invalid',
            'no_stock' => -5,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testSet_InvalidJson_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'set',
            '_format' => 'json',
        ));

        $client->request('PUT', $url, array(), array(), array(), '{malformed_json');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testSet_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'set',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'id' => 'sing-pencil',
            'no_stock' => 5,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $contentArray = json_decode($responseContent, true);
        $this->assertEquals(5, $contentArray['no_stock']);
    }

    public function testAdd_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'add',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'id' => 'sing-pencil',
            'no_stock' => 3,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $contentArray = json_decode($responseContent, true);
        $this->assertEquals(13, $contentArray['no_stock']);
    }

    public function testReduce_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'reduce',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'id' => 'sing-pencil',
            'no_stock' => 2,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $contentArray = json_decode($responseContent, true);
        $this->assertEquals(8, $contentArray['no_stock']);
    }

    public function testSetProductWarehouse_NotExists_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'set',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'product' => array(
                'id' => 'calendar'
            ),
            'warehouse' => array(
                'id' => 'sing'
            ),
            'no_stock' => 5,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $contentArray = json_decode($responseContent, true);
        $this->assertEquals(5, $contentArray['no_stock']);
    }

    public function testSetProductWarehouse_Exists_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'set',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'product' => array(
                'id' => 'pencil'
            ),
            'warehouse' => array(
                'id' => 'sing'
            ),
            'no_stock' => 7,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $contentArray = json_decode($responseContent, true);
        $this->assertEquals(7, $contentArray['no_stock']);
    }

    public function testAddProductWarehouse_NotExists_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'add',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'product' => array(
                'id' => 'calendar'
            ),
            'warehouse' => array(
                'id' => 'sing'
            ),
            'no_stock' => 5,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $contentArray = json_decode($responseContent, true);
        $this->assertEquals(5, $contentArray['no_stock']);
    }

    public function testAddProductWarehouse_Exists_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'add',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'product' => array(
                'id' => 'pencil'
            ),
            'warehouse' => array(
                'id' => 'sing'
            ),
            'no_stock' => 5,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $contentArray = json_decode($responseContent, true);
        $this->assertEquals(15, $contentArray['no_stock']);
    }

    public function testReduceProductWarehouse_NotExists_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'reduce',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'product' => array(
                'id' => 'calendar'
            ),
            'warehouse' => array(
                'id' => 'sing'
            ),
            'no_stock' => 5,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testReduceProductWarehouse_Exists_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'reduce',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'product' => array(
                'id' => 'pencil'
            ),
            'warehouse' => array(
                'id' => 'sing'
            ),
            'no_stock' => 15,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testReduceProductWarehouse_Exists_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('inventory_set', array(
            'mode' => 'reduce',
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'product' => array(
                'id' => 'pencil'
            ),
            'warehouse' => array(
                'id' => 'sing'
            ),
            'no_stock' => 2,
        ));

        $client->request('PUT', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $contentArray = json_decode($responseContent, true);
        $this->assertEquals(8, $contentArray['no_stock']);
    }
}
