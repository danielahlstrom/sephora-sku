<?php
namespace Tests\Sephora\SkuBundle\Controller;

use Liip\FunctionalTestBundle\Test\WebTestCase;

/**
 * Class ProductControllerTest
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Controller
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class ProductControllerTest extends WebTestCase
{
    public function setUp()
    {
        $this->loadFixtures(array('Sephora\SkuBundle\DataFixtures\ORM\SkuTestFixtures'));
    }

    public function testList_500()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('products_list', array(
            '_format' => 'json',
            'page' => -1,
        ));

        $client->request('GET', $url);
        $this->assertEquals(500, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testList_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('products_list', array(
            '_format' => 'json',
            'page' => 0,
            'page_size' => 10,
        ));

        $client->request('GET', $url);
        $this->isSuccessful($client->getResponse());
        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
        $this->assertCount(10, json_decode($responseContent, true));
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testSingle_404()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('product_get', array(
            'id' => 'invalid',
            '_format' => 'json',
        ));

        $client->request('GET', $url);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testSingle_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('product_get', array(
            'id' => 'pencil',
            '_format' => 'json',
        ));

        $client->request('GET', $url);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
    }

    public function testInventories_404()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('product_inventories', array(
            'id' => 'invalid',
            '_format' => 'json',
        ));

        $client->request('GET', $url);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testInventories_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('product_inventories', array(
            'id' => 'pencil',
            '_format' => 'json',
        ));

        $client->request('GET', $url);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
    }

    public function testReservations_404()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('product_reservations', array(
            'id' => 'invalid',
            '_format' => 'json',
        ));

        $client->request('GET', $url);
        $this->assertEquals(404, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testReservations_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('product_reservations', array(
            'id' => 'pencil',
            '_format' => 'json',
        ));

        $client->request('GET', $url);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
    }

    public function testCreate_400()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('product_create', array(
            '_format' => 'json',
        ));

        $client->request('POST', $url, array(), array(), array(), '{malformed_json');
        $this->assertEquals(400, $client->getResponse()->getStatusCode());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));
    }

    public function testCreate_200()
    {
        $client = $this->makeClient();
        $url = $this->getUrl('product_create', array(
            '_format' => 'json',
        ));

        $content = json_encode(array(
            'name' => 'Test',
        ));

        $client->request('POST', $url, array(), array(), array(), $content);
        $this->isSuccessful($client->getResponse());
        $this->assertEquals('application/json', $client->getResponse()->headers->get('Content-Type'));

        $responseContent = $client->getResponse()->getContent();
        $this->assertJson($responseContent);
    }
}
