<?php
namespace Tests\Sephora\SkuBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Inventory as InventoryEntity;
use Sephora\SkuBundle\Entity\Product as ProductEntity;
use Sephora\SkuBundle\Entity\Warehouse as WarehouseEntity;
use Sephora\SkuBundle\Repository\InventoryRepository;
use Sephora\SkuBundle\Service\Inventory;
use Sephora\SkuBundle\Service\Serializer;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Inventory
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class InventoryTest extends TestCase
{
    /**
     * @var Inventory
     */
    protected $inventoryService;

    protected $requestMock;

    public function setUp()
    {
        $this->inventoryService = new Inventory();

        $this->requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getContent', 'getRequestFormat'))
            ->getMock();
        $this->requestMock->expects($this->any())
            ->method('getContent')
            ->willReturn('[]');
        $this->requestMock->expects($this->any())
            ->method('getRequestFormat')
            ->willReturn('json');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUpdate_InvalidMode()
    {
        $this->inventoryService->updateInventory($this->requestMock, 'invalid');
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUpdate_InvalidFormat()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new UnsupportedFormatException());

        $this->inventoryService->setSerializer($serializerMock);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUpdate_InvalidJson()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new RuntimeException());

        $this->inventoryService->setSerializer($serializerMock);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
    }

    /**
     * @expectedException \Exception
     */
    public function testUpdate_DeserializeException()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new \Exception());

        $this->inventoryService->setSerializer($serializerMock);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
    }

    /**
     * @expectedException \Sephora\SkuBundle\Exception\NegativeCountException
     */
    public function testUpdate_NegativeStock()
    {
        $inventory = new InventoryEntity();
        $inventory->setNoStock(-3);
        $this->setSerializerMockWithInventory($inventory);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testUpdate_IdNotFound()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $inventory = new InventoryEntity();
        $inventory->generateId();
        $inventory->setNoStock(3);
        $this->setSerializerMockWithInventory($inventory);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
    }

    /**
     * @expectedException \Sephora\SkuBundle\Exception\NegativeCountException
     */
    public function testUpdate_ProductWarehouse()
    {
        $repositoryMock = $this->getRepositoryMock(array('findOneBy'));
        $repositoryMock->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $entityManagerMock = $this->getEntityManagerMock(array('getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $inventory = new InventoryEntity();
        $inventory->setNoStock(3);
        $inventory->setProduct(new ProductEntity());
        $inventory->setWarehouse(new WarehouseEntity());
        $this->setSerializerMockWithInventory($inventory);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_REDUCE);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testUpdate_Create_ProductWarehouseNull()
    {
        $repositoryMock = $this->getRepositoryMock(array('findOneBy'));
        $repositoryMock->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $entityManagerMock = $this->getEntityManagerMock(array('getRepository', 'find'));
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);
        $entityManagerMock->expects($this->exactly(2))
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $inventory = new InventoryEntity();
        $inventory->setNoStock(3);
        $inventory->setProduct(new ProductEntity());
        $inventory->setWarehouse(new WarehouseEntity());
        $this->setSerializerMockWithInventory($inventory);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
    }

    /**
     * @expectedException \Doctrine\ORM\OptimisticLockException
     */
    public function testUpdate_Create_LockFail()
    {
        $repositoryMock = $this->getRepositoryMock(array('findOneBy'));
        $repositoryMock->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $entityManagerMock = $this->getEntityManagerMock(array('getRepository', 'find', 'persist', 'lock'));
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);
        $entityManagerMock->expects($this->exactly(2))
            ->method('find')
            ->willReturn(new ProductEntity(), new WarehouseEntity());
        $entityManagerMock->expects($this->once())
            ->method('persist');
        $entityManagerMock->expects($this->once())
            ->method('lock')
            ->willThrowException(new OptimisticLockException('', new InventoryEntity()));

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $inventory = new InventoryEntity();
        $inventory->setNoStock(3);
        $inventory->setProduct(new ProductEntity());
        $inventory->setWarehouse(new WarehouseEntity());
        $this->setSerializerMockWithInventory($inventory);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
    }

    public function testUpdate_Create_Success()
    {
        $repositoryMock = $this->getRepositoryMock(array('findOneBy'));
        $repositoryMock->expects($this->once())
            ->method('findOneBy')
            ->willReturn(null);

        $entityManagerMock = $this->getEntityManagerMock(array('getRepository', 'find', 'persist', 'lock', 'flush'));
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);
        $entityManagerMock->expects($this->exactly(2))
            ->method('find')
            ->willReturn(new ProductEntity(), new WarehouseEntity());
        $entityManagerMock->expects($this->once())
            ->method('persist');
        $entityManagerMock->expects($this->once())
            ->method('lock');
        $entityManagerMock->expects($this->once())
            ->method('flush');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $inventory = new InventoryEntity();
        $inventory->setNoStock(3);
        $inventory->setProduct(new ProductEntity());
        $inventory->setWarehouse(new WarehouseEntity());
        $this->setSerializerMockWithInventory($inventory);

        $response = $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
        $this->assertTrue($response instanceof InventoryEntity);
    }

    /**
     * @expectedException \Exception
     */
    public function testUpdate_IdLockFail()
    {
        $inventory = new InventoryEntity();
        $inventory->generateId();
        $inventory->setNoStock(3);

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn($inventory);
        $entityManagerMock->expects($this->once())
            ->method('lock')
            ->willThrowException(new OptimisticLockException('', new InventoryEntity()));

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $this->setSerializerMockWithInventory($inventory);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
    }

    public function testUpdate_Set_Success()
    {
        $inventory = new InventoryEntity();
        $inventory->generateId();
        $inventory->setNoStock(3);

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock', 'flush'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn($inventory);
        $entityManagerMock->expects($this->once())
            ->method('lock');
        $entityManagerMock->expects($this->once())
            ->method('flush');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $this->setSerializerMockWithInventory($inventory);

        $response = $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_SET);
        $this->assertTrue($response instanceof InventoryEntity);
        $this->assertEquals(3, $response->getNoStock());
    }

    public function testUpdate_Add_Success()
    {
        $inventory = new InventoryEntity();
        $inventory->setNoStock(3);

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock', 'flush'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn($inventory);
        $entityManagerMock->expects($this->once())
            ->method('lock');
        $entityManagerMock->expects($this->once())
            ->method('flush');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $inventoryAdd = new InventoryEntity();
        $inventoryAdd->generateId();
        $inventoryAdd->setNoStock(2);
        $this->setSerializerMockWithInventory($inventoryAdd);

        $response = $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_ADD);
        $this->assertTrue($response instanceof InventoryEntity);
        $this->assertEquals(5, $response->getNoStock());
    }

    public function testUpdate_Reduce_Success()
    {
        $inventory = new InventoryEntity();
        $inventory->setNoStock(3);

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock', 'flush'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn($inventory);
        $entityManagerMock->expects($this->once())
            ->method('lock');
        $entityManagerMock->expects($this->once())
            ->method('flush');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $inventoryAdd = new InventoryEntity();
        $inventoryAdd->generateId();
        $inventoryAdd->setNoStock(2);
        $this->setSerializerMockWithInventory($inventoryAdd);

        $response = $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_REDUCE);
        $this->assertTrue($response instanceof InventoryEntity);
        $this->assertEquals(1, $response->getNoStock());
    }

    /**
     * @expectedException \Sephora\SkuBundle\Exception\NegativeCountException
     */
    public function testUpdate_Reduce_NegativeStock()
    {
        $inventory = new InventoryEntity();
        $inventory->setNoStock(3);

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock', 'flush'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn($inventory);
        $entityManagerMock->expects($this->once())
            ->method('lock');
        $entityManagerMock->expects($this->never())
            ->method('flush');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);
        $this->inventoryService->setDoctrine($doctrineRegistryMock);

        $inventoryAdd = new InventoryEntity();
        $inventoryAdd->generateId();
        $inventoryAdd->setNoStock(4);
        $this->setSerializerMockWithInventory($inventoryAdd);

        $this->inventoryService->updateInventory($this->requestMock, Inventory::MODE_REDUCE);
    }

    /*
     * Protected "helper" methods
     */

    /**
     * @param InventoryEntity $inventory
     */
    protected function setSerializerMockWithInventory(InventoryEntity $inventory)
    {
        $serializerMock = $this->getMockBuilder(Serializer::class)
            ->disableOriginalConstructor()
            ->setMethods(array('deserialize'))
            ->getMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn($inventory);

        $this->inventoryService->setSerializer($serializerMock);
    }

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getRepositoryMock(array $methods)
    {
        return $this->getMockBuilder(InventoryRepository::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getEntityManagerMock(array $methods)
    {
        return $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $entityManagerMock
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getDoctrineRegistryMock($entityManagerMock)
    {
        $doctrineRegistryMock = $this->getMockBuilder(Registry::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getManager'))
            ->getMock();
        $doctrineRegistryMock->expects($this->once())
            ->method('getManager')
            ->willReturn($entityManagerMock);

        return $doctrineRegistryMock;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getSerializerMock()
    {
        return $this->getMockBuilder(Serializer::class)
            ->disableOriginalConstructor()
            ->setMethods(array('deserialize'))
            ->getMock();
    }
}
