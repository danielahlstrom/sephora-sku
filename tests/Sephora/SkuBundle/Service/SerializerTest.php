<?php
namespace Tests\Sephora\SkuBundle\Service;

use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Product;
use Sephora\SkuBundle\Service\Serializer;

/**
 * Class Serializer
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class SerializerTest extends TestCase
{
    /**
     * @var Serializer
     */
    protected $serializer;

    public function setUp()
    {
        $cacheDir = __DIR__.'/../../../../var/cache/test';
        $this->serializer = new Serializer($cacheDir, true);
    }

    /**
     * Test if groups is set correctly in a SerializationContext
     */
    public function testGetSerializationContext()
    {
        $serializationContext1 = $this->serializer->getSerializationContext(array('group1', 'group2'));
        $this->assertTrue($serializationContext1 instanceof SerializationContext);
        $this->assertEquals(array('group1', 'group2'), $serializationContext1->attributes->get('groups')->get());

        $serializationContext2 = $this->serializer->getSerializationContext('group');
        $this->assertTrue($serializationContext2 instanceof SerializationContext);
        $this->assertEquals(array('group'), $serializationContext2->attributes->get('groups')->get());
    }

    /**
     * Test if getSerializationContext throws an exception if groups are empty
     *
     * @expectedException \LogicException
     */
    public function testGetSerializationContextEmptyGroups()
    {
        $this->serializer->getSerializationContext(null);
    }

    /**
     * Test if groups is set correctly in a DeserializationContext
     */
    public function testGetDeserializationContext()
    {
        $deserializationContext1 = $this->serializer->getDeserializationContext(array('group1', 'group2'));
        $this->assertTrue($deserializationContext1 instanceof DeserializationContext);
        $this->assertEquals(array('group1', 'group2'), $deserializationContext1->attributes->get('groups')->get());

        $deserializationContext2 = $this->serializer->getDeserializationContext('group');
        $this->assertTrue($deserializationContext2 instanceof DeserializationContext);
        $this->assertEquals(array('group'), $deserializationContext2->attributes->get('groups')->get());
    }

    /**
     * Test if getDeserializationContext throws an exception if groups are empty
     *
     * @expectedException \LogicException
     */
    public function testGetDeserializationContextEmptyGroups()
    {
        $this->serializer->getDeserializationContext(null);
    }

    public function testSerializeEntity()
    {
        $entity = new Product();
        $entity->setName('Test Name');

        $json = $this->serializer->serialize($entity, 'json');
        $this->assertJson($json);
        $this->assertEquals(json_encode(array('name' => 'Test Name')), $json);
    }

    public function testDeserializeEntity()
    {
        $json = json_encode(array('name' => 'Test Name'));
        $entity = $this->serializer->deserialize($json, Product::class, 'json');
        $this->assertTrue($entity instanceof Product);
    }
}
