<?php
namespace Tests\Sephora\SkuBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Inventory as InventoryEntity;
use Sephora\SkuBundle\Entity\Product as ProductEntity;
use Sephora\SkuBundle\Entity\Reservation as ReservationEntity;
use Sephora\SkuBundle\Repository\InventoryRepository;
use Sephora\SkuBundle\Repository\ReservationRepository;
use Sephora\SkuBundle\Service\Product;
use Sephora\SkuBundle\Service\Serializer;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Product
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class ProductTest extends TestCase
{
    public function testGetProducts_Success()
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();
        $requestMock->query = new ParameterBag(array(
            'page' => 0,
            'page_size' => 10,
        ));

        $repositoryMock = $this->getRepositoryMock(array('findBy'));
        $repositoryMock->expects($this->once())
            ->method('findBy')
            ->willReturn(array(
                new ProductEntity(),
                new ProductEntity(),
            ));

        $entityManagerMock = $this->getEntityManagerMock(array('getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $productService = new Product();
        $productService->setDoctrine($doctrineRegistryMock);

        $response = $productService->getProducts($requestMock);
        $this->assertCount(2, $response);
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testGetProduct_Null()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $productService = new Product();
        $productService->setDoctrine($doctrineRegistryMock);

        $productService->getProduct('test-id');
    }

    public function testGetProduct_Success()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(new ProductEntity());

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $productService = new Product();
        $productService->setDoctrine($doctrineRegistryMock);

        $response = $productService->getProduct('test-id');
        $this->assertTrue($response instanceof ProductEntity);
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testGetInventories_Null()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $productService = new Product();
        $productService->setDoctrine($doctrineRegistryMock);

        $response = $productService->getInventories('test-id');
        $this->assertCount(2, $response);
    }

    public function testGetInventories_Success()
    {
        $repositoryMock = $this->getMockBuilder(InventoryRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getInventoriesForProduct'))
            ->getMock();
        $repositoryMock->expects($this->once())
            ->method('getInventoriesForProduct')
            ->willReturn(array(
                new InventoryEntity(),
                new InventoryEntity(),
            ));

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(new Product());
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $productService = new Product();
        $productService->setDoctrine($doctrineRegistryMock);

        $response = $productService->getInventories('test-id');
        $this->assertCount(2, $response);
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testGetReservations_Null()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $productService = new Product();
        $productService->setDoctrine($doctrineRegistryMock);

        $response = $productService->getReservations('test-id');
        $this->assertCount(2, $response);
    }

    public function testGetReservations_Success()
    {
        $repositoryMock = $this->getMockBuilder(ReservationRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getReservationsForProduct'))
            ->getMock();
        $repositoryMock->expects($this->once())
            ->method('getReservationsForProduct')
            ->willReturn(array(
                new ReservationEntity(),
                new ReservationEntity(),
            ));

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(new Product());
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $productService = new Product();
        $productService->setDoctrine($doctrineRegistryMock);

        $response = $productService->getReservations('test-id');
        $this->assertCount(2, $response);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateProduct_InvalidFormat()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new UnsupportedFormatException());

        $productService = new Product();
        $productService->setSerializer($serializerMock);

        $productService->createProduct($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateProduct_InvalidJson()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new RuntimeException());

        $productService = new Product();
        $productService->setSerializer($serializerMock);

        $productService->createProduct($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \Exception
     */
    public function testCreateProduct_Exception()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new \Exception());

        $productService = new Product();
        $productService->setSerializer($serializerMock);

        $productService->createProduct($this->getRequestMockForCreate());
    }

    public function testCreateProduct_Success()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn(new ProductEntity());

        $entityManagerMock = $this->getEntityManagerMock(array('persist', 'flush'));
        $entityManagerMock->expects($this->once())
            ->method('persist');
        $entityManagerMock->expects($this->once())
            ->method('flush');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $productService = new Product();
        $productService->setDoctrine($doctrineRegistryMock);
        $productService->setSerializer($serializerMock);

        $response = $productService->createProduct($this->getRequestMockForCreate());
        $this->assertTrue($response instanceof ProductEntity);
    }

    /*
     * Protected "helper" methods
     */

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getRepositoryMock(array $methods)
    {
        return $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getEntityManagerMock(array $methods)
    {
        return $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $entityManagerMock
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getDoctrineRegistryMock($entityManagerMock)
    {
        $doctrineRegistryMock = $this->getMockBuilder(Registry::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getManager'))
            ->getMock();
        $doctrineRegistryMock->expects($this->once())
            ->method('getManager')
            ->willReturn($entityManagerMock);

        return $doctrineRegistryMock;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getRequestMockForCreate()
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getContent', 'getRequestFormat'))
            ->getMock();
        $requestMock->expects($this->once())
            ->method('getContent')
            ->willReturn('[]');
        $requestMock->expects($this->once())
            ->method('getRequestFormat')
            ->willReturn('json');

        return $requestMock;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getSerializerMock()
    {
        return $this->getMockBuilder(Serializer::class)
            ->disableOriginalConstructor()
            ->setMethods(array('deserialize'))
            ->getMock();
    }
}
