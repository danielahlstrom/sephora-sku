<?php
namespace Tests\Sephora\SkuBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Inventory as InventoryEntity;
use Sephora\SkuBundle\Entity\Reservation as ReservationEntity;
use Sephora\SkuBundle\Entity\Warehouse as WarehouseEntity;
use Sephora\SkuBundle\Repository\InventoryRepository;
use Sephora\SkuBundle\Repository\ReservationRepository;
use Sephora\SkuBundle\Service\Serializer;
use Sephora\SkuBundle\Service\Warehouse;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Warehouse
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class WarehouseTest extends TestCase
{
    public function testGetCustomers_Success()
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();
        $requestMock->query = new ParameterBag(array(
            'page' => 0,
            'page_size' => 10,
        ));

        $repositoryMock = $this->getEntityManagerMock(array('findBy'));
        $repositoryMock->expects($this->once())
            ->method('findBy')
            ->willReturn(array(
                new WarehouseEntity(),
                new WarehouseEntity(),
            ));

        $entityManagerMock = $this->getEntityManagerMock(array('getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $warehouseService = new Warehouse();
        $warehouseService->setDoctrine($doctrineMock);

        $response = $warehouseService->getWarehouses($requestMock);

        $this->assertCount(2, $response);
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testGetInventories_Null()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $warehouseService = new Warehouse();
        $warehouseService->setDoctrine($doctrineRegistryMock);

        $response = $warehouseService->getInventories('test-id');
        $this->assertCount(2, $response);
    }

    public function testGetInventories_Success()
    {
        $repositoryMock = $this->getMockBuilder(InventoryRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getInventoriesForWarehouse'))
            ->getMock();
        $repositoryMock->expects($this->once())
            ->method('getInventoriesForWarehouse')
            ->willReturn(array(
                new InventoryEntity(),
                new InventoryEntity(),
            ));

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(new Warehouse());
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $warehouseService = new Warehouse();
        $warehouseService->setDoctrine($doctrineRegistryMock);

        $response = $warehouseService->getInventories('test-id');
        $this->assertCount(2, $response);
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testGetReservations_Null()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $warehouseService = new Warehouse();
        $warehouseService->setDoctrine($doctrineRegistryMock);

        $response = $warehouseService->getReservations('test-id');
        $this->assertCount(2, $response);
    }

    public function testGetReservations_Success()
    {
        $repositoryMock = $this->getMockBuilder(ReservationRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getReservationsForWarehouse'))
            ->getMock();
        $repositoryMock->expects($this->once())
            ->method('getReservationsForWarehouse')
            ->willReturn(array(
                new ReservationEntity(),
                new ReservationEntity(),
            ));

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(new Warehouse());
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $warehouseService = new Warehouse();
        $warehouseService->setDoctrine($doctrineRegistryMock);

        $response = $warehouseService->getReservations('test-id');
        $this->assertCount(2, $response);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateWarehouse_InvalidFormat()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new UnsupportedFormatException());

        $warehouseService = new Warehouse();
        $warehouseService->setSerializer($serializerMock);

        $warehouseService->createWarehouse($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateWarehouse_InvalidJson()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new RuntimeException());

        $warehouseService = new Warehouse();
        $warehouseService->setSerializer($serializerMock);

        $warehouseService->createWarehouse($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \Exception
     */
    public function testCreateWarehouse_Exception()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new \Exception());

        $warehouseService = new Warehouse();
        $warehouseService->setSerializer($serializerMock);

        $warehouseService->createWarehouse($this->getRequestMockForCreate());
    }

    public function testCreateWarehouse_Success()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn(new WarehouseEntity());

        $entityManagerMock = $this->getEntityManagerMock(array('persist', 'flush'));
        $entityManagerMock->expects($this->once())
            ->method('persist');
        $entityManagerMock->expects($this->once())
            ->method('flush');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $warehouseService = new Warehouse();
        $warehouseService->setDoctrine($doctrineRegistryMock);
        $warehouseService->setSerializer($serializerMock);

        $response = $warehouseService->createWarehouse($this->getRequestMockForCreate());
        $this->assertTrue($response instanceof WarehouseEntity);
    }

    /*
     * Protected "helper" methods
     */

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getRepositoryMock(array $methods)
    {
        return $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getEntityManagerMock(array $methods)
    {
        return $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $entityManagerMock
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getDoctrineRegistryMock($entityManagerMock)
    {
        $doctrineRegistryMock = $this->getMockBuilder(Registry::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getManager'))
            ->getMock();
        $doctrineRegistryMock->expects($this->once())
            ->method('getManager')
            ->willReturn($entityManagerMock);

        return $doctrineRegistryMock;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getRequestMockForCreate()
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getContent', 'getRequestFormat'))
            ->getMock();
        $requestMock->expects($this->once())
            ->method('getContent')
            ->willReturn('[]');
        $requestMock->expects($this->once())
            ->method('getRequestFormat')
            ->willReturn('json');

        return $requestMock;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getSerializerMock()
    {
        return $this->getMockBuilder(Serializer::class)
            ->disableOriginalConstructor()
            ->setMethods(array('deserialize'))
            ->getMock();
    }
}
