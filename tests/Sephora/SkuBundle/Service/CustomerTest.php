<?php
namespace Tests\Sephora\SkuBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Customer as CustomerEntity;
use Sephora\SkuBundle\Entity\Reservation as ReservationEntity;
use Sephora\SkuBundle\Repository\ReservationRepository;
use Sephora\SkuBundle\Service\Customer;
use Sephora\SkuBundle\Service\Serializer;
use Symfony\Component\HttpFoundation\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Customer
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class CustomerTest extends TestCase
{
    public function testGetCustomers_Success()
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();
        $requestMock->query = new ParameterBag(array(
            'page' => 0,
            'page_size' => 10,
        ));

        $repositoryMock = $this->getRepositoryMock(array('findBy'));
        $repositoryMock->expects($this->once())
            ->method('findBy')
            ->willReturn(array(
                new CustomerEntity(),
                new CustomerEntity(),
            ));

        $entityManagerMock = $this->getEntityManagerMock(array('getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $customerService = new Customer();
        $customerService->setDoctrine($doctrineMock);

        $response = $customerService->getCustomers($requestMock);

        $this->assertCount(2, $response);
    }

    public function testGetCustomer_Success()
    {
        $customerEntity = new CustomerEntity();

        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn($customerEntity);

        $doctrineMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $customerService = new Customer();
        $customerService->setDoctrine($doctrineMock);

        $response = $customerService->getCustomer('test-id');
        $this->assertEquals($customerEntity, $response);
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testGetCustomer_Null()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);
        $doctrineMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $customerService = new Customer();
        $customerService->setDoctrine($doctrineMock);
        $customerService->getCustomer('test-id');
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testGetReservations_Null()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $doctrineMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $customerService = new Customer();
        $customerService->setDoctrine($doctrineMock);
        $response = $customerService->getReservations('test-id');
        $this->assertCount(2, $response);
    }

    public function testGetReservations_Success()
    {
        $repositoryMock = $this->getMockBuilder(ReservationRepository::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getReservationsForCustomer'))
            ->getMock();
        $repositoryMock->expects($this->once())
            ->method('getReservationsForCustomer')
            ->willReturn(array(
                new ReservationEntity(),
                new ReservationEntity(),
            ));

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(new Customer());
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $customerService = new Customer();
        $customerService->setDoctrine($doctrineMock);
        $response = $customerService->getReservations('test-id');
        $this->assertCount(2, $response);
    }

    public function testCreateCustomer_Success()
    {
        $requestMock = $this->getRequestMock();

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn(new CustomerEntity());

        $entityManagerMock = $this->getEntityManagerMock(array('persist', 'flush'));
        $entityManagerMock->expects($this->once())
            ->method('persist');
        $entityManagerMock->expects($this->once())
            ->method('flush');

        $doctrineMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $customerService = new Customer();
        $customerService->setDoctrine($doctrineMock);
        $customerService->setSerializer($serializerMock);

        $response = $customerService->createCustomer($requestMock);

        $this->assertTrue($response instanceof CustomerEntity);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateCustomer_InvalidFormat()
    {
        $requestMock = $this->getRequestMock();

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new UnsupportedFormatException());

        $customerService = new Customer();
        $customerService->setSerializer($serializerMock);

        $customerService->createCustomer($requestMock);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateCustomer_InvalidJson()
    {
        $requestMock = $this->getRequestMock();

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new RuntimeException());

        $customerService = new Customer();
        $customerService->setSerializer($serializerMock);

        $customerService->createCustomer($requestMock);
    }

    /**
     * @expectedException \Exception
     */
    public function testCreateCustomer_Exception()
    {
        $requestMock = $this->getRequestMock();

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new \Exception());

        $customerService = new Customer();
        $customerService->setSerializer($serializerMock);

        $customerService->createCustomer($requestMock);
    }

    /*
     * Protected "helper" methods
     */

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getRepositoryMock(array $methods)
    {
        return $this->getMockBuilder(EntityRepository::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getEntityManagerMock(array $methods)
    {
        return $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $entityManagerMock
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getDoctrineRegistryMock($entityManagerMock)
    {
        $doctrineRegistryMock = $this->getMockBuilder(Registry::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getManager'))
            ->getMock();
        $doctrineRegistryMock->expects($this->once())
            ->method('getManager')
            ->willReturn($entityManagerMock);

        return $doctrineRegistryMock;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getSerializerMock()
    {
        return $this->getMockBuilder(Serializer::class)
            ->disableOriginalConstructor()
            ->setMethods(array('deserialize'))
            ->getMock();
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getRequestMock()
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getContent', 'getRequestFormat'))
            ->getMock();
        $requestMock->expects($this->once())
            ->method('getContent')
            ->willReturn('[]');
        $requestMock->expects($this->once())
            ->method('getRequestFormat')
            ->willReturn('json');

        return $requestMock;
    }
}
