<?php
namespace Tests\Sephora\SkuBundle\Service;

use Doctrine\Bundle\DoctrineBundle\Registry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\OptimisticLockException;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use PHPUnit\Framework\TestCase;
use Sephora\SkuBundle\Entity\Customer as CustomerEntity;
use Sephora\SkuBundle\Entity\Inventory as InventoryEntity;
use Sephora\SkuBundle\Entity\Product as ProductEntity;
use Sephora\SkuBundle\Entity\Reservation as ReservationEntity;
use Sephora\SkuBundle\Entity\Warehouse as WarehouseEntity;
use Sephora\SkuBundle\Repository\ReservationRepository;
use Sephora\SkuBundle\Service\Reservation;
use Sephora\SkuBundle\Service\Serializer;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Reservation
 * Creation date: 2018-03-31
 *
 * @package Tests\Sephora\SkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class ReservationTest extends TestCase
{
    public function testGetReservations_Success()
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();
        $requestMock->query = new ParameterBag(array(
            'page' => 0,
            'page_size' => 10,
        ));

        $repositoryMock = $this->getEntityManagerMock(array('getReservations'));
        $repositoryMock->expects($this->once())
            ->method('getReservations')
            ->willReturn(array(
                new ReservationEntity(),
                new ReservationEntity(),
            ));

        $entityManagerMock = $this->getEntityManagerMock(array('getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineMock);

        $response = $reservationService->getReservations($requestMock);

        $this->assertCount(2, $response);
    }

    public function testGetReservations_Csv_Success()
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->getMock();
        $requestMock->query = new ParameterBag(array(
            'page' => 0,
            'page_size' => 10,
        ));

        $reservations = array();
        for ($i = 0; $i < 5; $i += 1) {
            $reservation = new ReservationEntity();
            $reservation->generateId();
            $reservation->setNoItems($i);

            $customer = new CustomerEntity();
            $customer->generateId();
            $customer->setName('Customer '.$i);
            $reservation->setCustomer($customer);

            $inventory = new InventoryEntity();
            $inventory->setNoStock($i);
            $inventory->setNoReserved($i);

            $product = new ProductEntity();
            $product->generateId();
            $product->setName('Product '.$i);

            $warehouse = new WarehouseEntity();
            $warehouse->generateId();
            $warehouse->setName('Warehouse '.$i);

            $inventory->setProduct($product);
            $inventory->setWarehouse($warehouse);

            $reservation->setInventory($inventory);

            $reservations[] = $reservation;
        }

        $repositoryMock = $this->getEntityManagerMock(array('getReservations'));
        $repositoryMock->expects($this->once())
            ->method('getReservations')
            ->willReturn($reservations);

        $entityManagerMock = $this->getEntityManagerMock(array('getRepository'));
        $entityManagerMock->expects($this->once())
            ->method('getRepository')
            ->willReturn($repositoryMock);

        $doctrineMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineMock);

        $csv = $reservationService->getReservationsAsCsv($requestMock);

        $this->assertEquals(5, substr_count($csv, PHP_EOL) + 1);
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testGetReservation_Null()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);

        $reservationService->getReservation('test-id');
    }

    public function testGetReservation_Success()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(new ReservationEntity());

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);

        $response = $reservationService->getReservation('test-id');
        $this->assertTrue($response instanceof ReservationEntity);
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateReservation_InvalidFormat()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new UnsupportedFormatException());

        $reservationService = new Reservation();
        $reservationService->setSerializer($serializerMock);

        $reservationService->createReservation($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateReservation_InvalidJson()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new RuntimeException());

        $reservationService = new Reservation();
        $reservationService->setSerializer($serializerMock);

        $reservationService->createReservation($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \Exception
     */
    public function testCreateReservation_Exception()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willThrowException(new \Exception());

        $reservationService = new Reservation();
        $reservationService->setSerializer($serializerMock);

        $reservationService->createReservation($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateReservation_NoCustomerInventory()
    {
        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn(new ReservationEntity());

        $reservationService = new Reservation();
        $reservationService->setSerializer($serializerMock);

        $reservationService->createReservation($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateReservation_ItemsFloat()
    {
        $reservation = new ReservationEntity();
        $reservation->setCustomer(new CustomerEntity());
        $reservation->setInventory(new InventoryEntity());
        $reservation->setNoItems(0.5);

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn($reservation);

        $reservationService = new Reservation();
        $reservationService->setSerializer($serializerMock);

        $reservationService->createReservation($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \Sephora\SkuBundle\Exception\NegativeCountException
     */
    public function testCreateReservation_NegativeCount()
    {
        $reservation = new ReservationEntity();
        $reservation->setCustomer(new CustomerEntity());
        $reservation->setInventory(new InventoryEntity());
        $reservation->setNoItems(-5);

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn($reservation);

        $reservationService = new Reservation();
        $reservationService->setSerializer($serializerMock);

        $reservationService->createReservation($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \InvalidArgumentException
     */
    public function testCreateReservation_CustomerInventoryNull()
    {
        $reservation = new ReservationEntity();
        $reservation->setCustomer(new CustomerEntity());
        $reservation->setInventory(new InventoryEntity());
        $reservation->setNoItems(5);

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn($reservation);

        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->exactly(2))
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);
        $reservationService->setSerializer($serializerMock);

        $reservationService->createReservation($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \Exception
     */
    public function testCreateReservation_LockFail()
    {
        $reservation = new ReservationEntity();
        $reservation->setCustomer(new CustomerEntity());
        $reservation->setInventory(new InventoryEntity());
        $reservation->setNoItems(5);

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn($reservation);

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock'));
        $entityManagerMock->expects($this->exactly(2))
            ->method('find')
            ->willReturn(new CustomerEntity(), new InventoryEntity());
        $entityManagerMock->expects($this->once())
            ->method('lock')
            ->willThrowException(new OptimisticLockException('', new InventoryEntity()));

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);
        $reservationService->setSerializer($serializerMock);

        $reservationService->createReservation($this->getRequestMockForCreate());
    }

    /**
     * @expectedException \Sephora\SkuBundle\Exception\TooManyReservedException
     */
    public function testCreateReservation_ReservedTooMany()
    {
        $reservation = new ReservationEntity();
        $reservation->setCustomer(new CustomerEntity());
        $reservation->setInventory(new InventoryEntity());
        $reservation->setNoItems(5);

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn($reservation);

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock'));
        $entityManagerMock->expects($this->exactly(2))
            ->method('find')
            ->willReturn(new CustomerEntity(), new InventoryEntity());
        $entityManagerMock->expects($this->once())
            ->method('lock');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);
        $reservationService->setSerializer($serializerMock);

        $reservationService->createReservation($this->getRequestMockForCreate());
    }

    public function testCreateReservation_Success()
    {
        $reservation = new ReservationEntity();
        $reservation->setCustomer(new CustomerEntity());
        $reservation->setInventory(new InventoryEntity());
        $reservation->setNoItems(5);

        $serializerMock = $this->getSerializerMock();
        $serializerMock->expects($this->once())
            ->method('deserialize')
            ->willReturn($reservation);

        $inventory = new InventoryEntity();
        $inventory->setNoStock(10);
        $inventory->setNoReserved(2);

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock', 'persist', 'flush'));
        $entityManagerMock->expects($this->exactly(2))
            ->method('find')
            ->willReturn(new CustomerEntity(), $inventory);
        $entityManagerMock->expects($this->once())
            ->method('lock');
        $entityManagerMock->expects($this->once())
            ->method('persist');
        $entityManagerMock->expects($this->once())
            ->method('flush');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);
        $reservationService->setSerializer($serializerMock);

        $response = $reservationService->createReservation($this->getRequestMockForCreate());
        $this->assertTrue($response instanceof ReservationEntity);
        $this->assertEquals(5, $response->getNoItems());
        $this->assertEquals(7, $response->getInventory()->getNoReserved());
    }

    /**
     * @expectedException \Doctrine\ORM\EntityNotFoundException
     */
    public function testShipReservation_Null()
    {
        $entityManagerMock = $this->getEntityManagerMock(array('find'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn(null);

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);

        $reservationService->shipReservation('test-id');
    }

    /**
     * @expectedException \Exception
     */
    public function testShipReservation_LockFail()
    {
        $reservation = new ReservationEntity();
        $reservation->setInventory(new InventoryEntity());

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn($reservation);
        $entityManagerMock->expects($this->once())
            ->method('lock')
            ->willThrowException(new OptimisticLockException('', new InventoryEntity()));

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);

        $reservationService->shipReservation('test-id');
    }

    /**
     * @expectedException \Sephora\SkuBundle\Exception\NegativeCountException
     */
    public function testShipReservation_NegativeCount()
    {
        $reservation = new ReservationEntity();
        $reservation->setNoItems(2);
        $reservation->setInventory(new InventoryEntity());

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn($reservation);
        $entityManagerMock->expects($this->once())
            ->method('lock');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);

        $reservationService->shipReservation('test-id');
    }

    public function testShipReservation_Success()
    {
        $inventory = new InventoryEntity();
        $inventory->setNoStock(5);
        $inventory->setNoReserved(3);

        $reservation = new ReservationEntity();
        $reservation->setNoItems(2);
        $reservation->setInventory($inventory);

        $entityManagerMock = $this->getEntityManagerMock(array('find', 'lock', 'remove', 'flush'));
        $entityManagerMock->expects($this->once())
            ->method('find')
            ->willReturn($reservation);
        $entityManagerMock->expects($this->once())
            ->method('lock');
        $entityManagerMock->expects($this->once())
            ->method('remove');
        $entityManagerMock->expects($this->once())
            ->method('flush');

        $doctrineRegistryMock = $this->getDoctrineRegistryMock($entityManagerMock);

        $reservationService = new Reservation();
        $reservationService->setDoctrine($doctrineRegistryMock);

        $reservationService->shipReservation('test-id');
    }

    /*
     * Protected "helper" methods
     */

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    public function getRepositoryMock(array $methods)
    {
        return $this->getMockBuilder(ReservationRepository::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param array $methods
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getEntityManagerMock(array $methods)
    {
        return $this->getMockBuilder(EntityManager::class)
            ->disableOriginalConstructor()
            ->setMethods($methods)
            ->getMock();
    }

    /**
     * @param \PHPUnit_Framework_MockObject_MockObject $entityManagerMock
     *
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getDoctrineRegistryMock($entityManagerMock)
    {
        $doctrineRegistryMock = $this->getMockBuilder(Registry::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getManager'))
            ->getMock();
        $doctrineRegistryMock->expects($this->once())
            ->method('getManager')
            ->willReturn($entityManagerMock);

        return $doctrineRegistryMock;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getRequestMockForCreate()
    {
        $requestMock = $this->getMockBuilder(Request::class)
            ->disableOriginalConstructor()
            ->setMethods(array('getContent', 'getRequestFormat'))
            ->getMock();
        $requestMock->expects($this->once())
            ->method('getContent')
            ->willReturn('[]');
        $requestMock->expects($this->once())
            ->method('getRequestFormat')
            ->willReturn('json');

        return $requestMock;
    }

    /**
     * @return \PHPUnit_Framework_MockObject_MockObject
     */
    protected function getSerializerMock()
    {
        return $this->getMockBuilder(Serializer::class)
            ->disableOriginalConstructor()
            ->setMethods(array('deserialize'))
            ->getMock();
    }
}
