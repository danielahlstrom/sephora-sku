<?php
namespace Tests\Sephora\SkuBundle\DependencyInjection;

use Matthias\SymfonyDependencyInjectionTest\PhpUnit\AbstractExtensionTestCase;
use Sephora\SkuBundle\DependencyInjection\SephoraSkuExtension;
use Sephora\SkuBundle\Service\Customer;
use Sephora\SkuBundle\Service\Inventory;
use Sephora\SkuBundle\Service\Product;
use Sephora\SkuBundle\Service\Reservation;
use Sephora\SkuBundle\Service\Serializer;
use Sephora\SkuBundle\Service\Warehouse;
use Symfony\Component\DependencyInjection\Extension\ExtensionInterface;

/**
 * Class SephoraSkuExtensionTest
 * Creation date: 2018-04-02
 *
 * @package Tests\Sephora\SkuBundle\DependencyInjection
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class SephoraSkuExtensionTest extends AbstractExtensionTestCase
{
    /**
     * @return ExtensionInterface[]
     */
    protected function getContainerExtensions()
    {
        return array(new SephoraSkuExtension());
    }

    public function testLoad()
    {
        $this->load();

        $this->assertContainerBuilderHasService('sephora_sku.serializer', Serializer::class);
        $this->assertContainerBuilderHasService('sephora_sku.customer', Customer::class);
        $this->assertContainerBuilderHasService('sephora_sku.inventory', Inventory::class);
        $this->assertContainerBuilderHasService('sephora_sku.product', Product::class);
        $this->assertContainerBuilderHasService('sephora_sku.reservation', Reservation::class);
        $this->assertContainerBuilderHasService('sephora_sku.warehouse', Warehouse::class);
    }
}
