Sephora SKU assignment
===

[TOC]

# Installation

- Requires PHP 5.6 or later
- ``$ php composer.phar install -n`` in root directory
- ``$ bin/symfony_requirements`` to check for any additional requirements to run Symfony
- ``$ bin/console server:run localhost:8080`` to start an app.
- Open ``http://localhost:8080/config.php`` in the browser, to check the rest of the requirements to run Symfony
- Endpoints below will have base url ``http://localhost:8080``

# Tech used

##### PHP 5.6

I didn't want to spend time updating my development environment to PHP 7, since we still use PHP 5.6 as a base version at work and I use the same setup for since assignment.

##### Symfony 3

I choose Symfony 3 instead of Symfony 4 since Symfony 4 requires PHP 7. I'm also more comfortable with the structure of Symfony 2/3.

##### SQLite

This way I can include the database in the project. There should be no need to setup connections to another database. Only a few config value changes and additions should be needed to switch to any other SQL database.

# Limitations

I choose a few limitations to reduce the complexity of this assignment.

- Unlike a common shopping site I didn't include a shopping cart system.
- When a reservation is shipped, the reservation is deleted. Thereby there is no historical records.
- No logs of updates to inventories.
- This project is the central API to keep track of data. An interface for the distribution centers would need to be built to use this API as their data source.

# Entities and endpoints

Request ``{_format}`` parameter should be ``json`` if nothing else is mentioned.
 
Adding some annotations to entities could enable XML support.

## Customer

A customer can reserve products from a warehouse

#### Entity

Holds customer metadata and has a reference to it's reservations.

#### Endpoints

##### Create

Create a new customer.

``POST`` ``/customers.{_format}``  
Request content: ``{"name": "Luke Skywalker"}``

##### List

List all customers.

``GET`` ``/customers.{_format}``  
 ``page`` and ``page_size`` query parameters can be used for pagination. Defaults: ``page=0`` and ``page_size=100``.

##### Single

Get a single customer details.

``GET`` ``/customers/{id}.{_format}``  
 ``{id}`` is a customer id.
 
##### Reservations

List all reservations made by a customer.

``GET`` ``/customers/{id}/reservations.{_format}``  
 ``{id}`` is a customer id.

## Inventory

Inventory is the products in stock at a warehouse

#### Entity

Holds a product-warehouse pair which has a number of items in stock and number of items reserved.

#### Endpoints

##### Set, add or reduce

Set, add or reduce number of items in stock for a product in a warehouse. If no previous inventory is found for the product in the warehouse, an inventory is created.

Number of items in stock can't be negative, which means that ``{mode}``=``reduce`` will not create a new inventory. An error is returned if stock would be reduced to a negative amount.

``PUT`` ``/inventory/{mode}.{_format}``  
Request content:  
``{"id": "inventory-id", "no_stock": 5}``  
or ``{"product": {"id": "product-id"}, "warehouse": {"id": "warehouse-id"}, "no_stock": 5}``

## Product

A product is a sellable item

#### Entity

Holds metadata about a product.

#### Endpoints

##### Create

Create a new product.

``POST`` ``/products.{_format}``  
Request content: ``{"name": "Lightsaber"}``

##### List

List all products.

``GET`` ``/products.{_format}``  
 ``page`` and ``page_size`` query parameters can be used for pagination. Defaults: ``page=0`` and ``page_size=100``.
 
##### Reservations

List all reservations of a product.

``GET`` ``/products/{id}/reservations.{_format}``  
 ``{id}`` is a product id.

##### Inventories

List inventories for a product in all warehouses.

``GET`` ``/products/{id}/inventories.{_format}``  
 ``{id}`` is a product id.

## Reservation

Number of reserved items can not be more than what currently is in stock

#### Entity

Holds information about a customer and an inventory with number of items reserved.

#### Endpoints

##### Make reservation

Make a new reservation for an inventory item (a product in a warehouse).

``POST`` ``/reservations.{_format}``  
Request content: ``{"inventory": {"id": "inventory-id"}, "customer": {"id": "customer-id"}, "no_items": 1}``

##### List

``GET`` ``/reservations.{_format}``  
``_format`` can be either ``json`` or ``csv``.  
 ``page`` and ``page_size`` query parameters can be used for pagination. Defaults: ``page=0`` and ``page_size=100``.

##### Single

Get reservation details.

``GET`` ``/reservations/{id}.{_format}``  
 ``{id}`` is a reservation id.
 
##### Ship reservation

Ship a reservation. This will decrease the number of items reserved and in stock for an inventory, and will also delete the reservation.

``PUT`` ``/reservations/{id}/ship.{_format}``  
``{id}`` is a reservation id.

## Warehouse

A warehouse is a distribution center

#### Entity

Holds metadata about a warehouse.

#### Endpoints

##### Create

Create a new warehouse.

``POST`` ``/warehouses.{_format}``  
Request content: ``{"name": "Tatooine"}``

##### List

List all warehouses.

``GET`` ``/warehouses.{_format}``  
 ``page`` and ``page_size`` query parameters can be used for pagination. Defaults: ``page=0`` and ``page_size=100``.

##### Inventories

List inventory for a warehouse.

``GET`` ``/warehouses/{id}/inventories.{_format}``  
 ``{id}`` is a warehouse id.

##### Reservations

List reservations made that for this warehouse.

``GET`` ``/warehouses/{id}/reservations.{_format}``  
 ``{id}`` is a warehouse id.

# Tests

Run tests  
``$ vendor/bin/phpunit``

Run tests and generate code coverage. Overview can be found at the url ``/_phpunit/index.html``.  
``$ vendor/bin/phpunit --coverage-html web/_phpunit``

Running tests generate 2 deprecation notices. Those are from a bundle used in testing to load fixture data.
