<?php
namespace Sephora\SkuBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sephora\SkuBundle\Exception\NegativeCountException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class DefaultController
 * Creation date: 2018-03-27
 *
 * @package Sephora\SkuBundle\Controller
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class InventoryController extends Controller
{
    /**
     * @param Request $request
     * @param string  $_format
     * @param string  $mode
     *
     * @return Response
     *
     * @Route(
     *     "/inventory/{mode}.{_format}",
     *     name="inventory_set",
     *     methods={"PUT"},
     *     requirements={"mode"="set|add|reduce", "_format"="json"}
     * )
     */
    public function setAction(Request $request, $_format, $mode)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $inventory = $this->get('sephora_sku.inventory')->updateInventory($request, $mode);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof EntityNotFoundException) {
                $statusCode = 404;
            } elseif ($ex instanceof \InvalidArgumentException) {
                $statusCode = 400;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedInventory = $serializer->serialize($inventory, $_format, $serializer->getSerializationContext('details'));

        $response = new Response($serializedInventory);
        $response->headers->set('Content-Type', $request->getMimeType($_format));

        return $response;
    }

    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return array
     */
    protected function getContentResponseHeader(Request $request, $_format)
    {
        return array('Content-Type' => $request->getMimeType($_format));
    }
}
