<?php
namespace Sephora\SkuBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class CustomerController
 * Creation date: 2018-03-27
 *
 * @package Sephora\SkuBundle\Controller
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class CustomerController extends Controller
{
    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return Response
     *
     * @Route("/customers.{_format}",
     *     name="customers_list",
     *     methods={"GET"},
     *     requirements={"_format"="json"}
     * )
     */
    public function listAction(Request $request, $_format)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $customers = $this->get('sephora_sku.customer')->getCustomers($request);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedCustomers = $serializer->serialize($customers, $_format, $serializer->getSerializationContext('list'));

        return new Response($serializedCustomers, 200, $this->getContentResponseHeader($request, $_format));
    }

    /**
     * @param Request $request
     * @param string  $_format
     * @param string  $id
     *
     * @return Response|JsonResponse
     * @throws \Exception
     *
     * @Route("/customers/{id}.{_format}",
     *     name="customer_get",
     *     methods={"GET"},
     *     requirements={"_format"="json"}
     * )
     */
    public function singleAction(Request $request, $_format, $id)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $customer = $this->get('sephora_sku.customer')->getCustomer($id);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof EntityNotFoundException) {
                $statusCode = 404;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedCustomer = $serializer->serialize($customer, $_format, $serializer->getSerializationContext('details'));

        return new Response($serializedCustomer, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     * @param string  $_format
     * @param string  $id
     *
     * @return Response
     * @throws \Exception
     *
     * @Route("/customers/{id}/reservations.{_format}",
     *     name="customer_reservations",
     *     methods={"GET"},
     *     requirements={"_format"="json"}
     * )
     */
    public function reservationsAction(Request $request, $_format, $id)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $reservations = $this->get('sephora_sku.customer')->getReservations($id);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof EntityNotFoundException) {
                $statusCode = 404;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedReservations = $serializer->serialize($reservations, $_format, $serializer->getSerializationContext('list'));

        return new Response($serializedReservations, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return Response|JsonResponse
     *
     * @Route("/customers.{_format}",
     *     name="customer_create",
     *     methods={"POST"},
     *     requirements={"_format"="json"}
     * )
     */
    public function createAction(Request $request, $_format)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $customer = $this->get('sephora_sku.customer')->createCustomer($request);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof \InvalidArgumentException) {
                $statusCode = 400;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedCustomer = $serializer->serialize($customer, $_format, $serializer->getSerializationContext('details'));

        return new Response($serializedCustomer, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return array
     */
    protected function getContentResponseHeader(Request $request, $_format)
    {
        return array('Content-Type' => $request->getMimeType($_format));
    }
}
