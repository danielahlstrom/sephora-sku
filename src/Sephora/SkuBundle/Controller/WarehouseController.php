<?php
namespace Sephora\SkuBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class WarehouseController
 * Creation date: 2018-03-27
 *
 * @package Sephora\SkuBundle\Controller
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class WarehouseController extends Controller
{
    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return Response
     *
     * @Route("/warehouses.{_format}",
     *     name="warehouses_list",
     *     methods={"GET"},
     *     requirements={"_format"="json"}
     * )
     */
    public function listAction(Request $request, $_format)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $warehouses = $this->get('sephora_sku.warehouse')->getWarehouses($request);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedWarehouses = $serializer->serialize($warehouses, $_format, $serializer->getSerializationContext('list'));

        return new Response($serializedWarehouses, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return Response
     *
     * @Route("/warehouses/{id}/inventories.{_format}",
     *     name="warehouses_inventories",
     *     methods={"GET"},
     *     requirements={"_format"="json"}
     * )
     */
    public function inventoriesAction(Request $request, $_format, $id)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $inventories = $this->get('sephora_sku.warehouse')->getInventories($id);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof EntityNotFoundException) {
                $statusCode = 404;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializer = $this->get('sephora_sku.serializer');
        $serializedInventories = $serializer->serialize($inventories, $_format, $serializer->getSerializationContext('list'));

        return new Response($serializedInventories, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return Response
     *
     * @Route("/warehouses/{id}/reservations.{_format}",
     *     name="warehouses_reservations",
     *     methods={"GET"},
     *     requirements={"_format"="json"}
     * )
     */
    public function reservationsAction(Request $request, $_format, $id)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $reservations = $this->get('sephora_sku.warehouse')->getReservations($id);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof EntityNotFoundException) {
                $statusCode = 404;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializer = $this->get('sephora_sku.serializer');
        $serializedReservations = $serializer->serialize($reservations, $_format, $serializer->getSerializationContext('list_res_for_wh'));

        return new Response($serializedReservations, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return Response
     *
     * @Route("/warehouses.{_format}",
     *     name="warehouse_create",
     *     methods={"POST"},
     *     requirements={"_format"="json"}
     * )
     */
    public function createAction(Request $request, $_format)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $warehouse = $this->get('sephora_sku.warehouse')->createWarehouse($request);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof \InvalidArgumentException) {
                $statusCode = 400;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedWarehouse = $serializer->serialize($warehouse, $_format, $serializer->getSerializationContext('details'));

        return new Response($serializedWarehouse, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return array
     */
    protected function getContentResponseHeader(Request $request, $_format)
    {
        return array('Content-Type' => $request->getMimeType($_format));
    }
}
