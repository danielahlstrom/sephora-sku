<?php
namespace Sephora\SkuBundle\Controller;

use Doctrine\ORM\EntityNotFoundException;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ReservationController
 * Creation date: 2018-03-30
 *
 * @package Sephora\SkuBundle\Controller
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class ReservationController extends Controller
{
    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return Response
     *
     * @Route("/reservations.{_format}",
     *     name="reservations_list",
     *     methods={"GET"},
     *     requirements={"_format"="json"}
     * )
     */
    public function listAction(Request $request, $_format)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $reservations = $this->get('sephora_sku.reservation')->getReservations($request);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedReservation = $serializer->serialize($reservations, $_format, $serializer->getSerializationContext('list'));

        return new Response($serializedReservation, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     *
     * @return Response
     *
     * @Route("/reservations.csv",
     *     name="reservations_list_csv",
     *     methods={"GET"}
     * )
     */
    public function listCsvAction(Request $request)
    {
        try {
            $csv = $this->get('sephora_sku.reservation')->getReservationsAsCsv($request);
        } catch (\Exception $ex) {
            $statusCode = 500;

            return new Response($ex->getMessage(), $statusCode, array('Content-Type' => 'text/plain'));
        }

        return new Response($csv, 200, array('Content-Type' => 'text/csv'));
    }

    /**
     * @param Request $request
     * @param string  $_format
     * @param string  $id
     *
     * @return Response
     *
     * @Route("/reservations/{id}.{_format}",
     *     name="reservation_get",
     *     methods={"GET"},
     *     requirements={"_format"="json"}
     * )
     */
    public function singleAction(Request $request, $_format, $id)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $reservation = $this->get('sephora_sku.reservation')->getReservation($id);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof EntityNotFoundException) {
                $statusCode = 404;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedReservation = $serializer->serialize($reservation, $_format, $serializer->getSerializationContext('details'));

        return new Response($serializedReservation, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return Response
     *
     * @Route("/reservations.{_format}",
     *     name="reservation_create",
     *     methods={"POST"},
     *     requirements={"_format"="json"}
     * )
     */
    public function makeReservationAction(Request $request, $_format)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $reservation = $this->get('sephora_sku.reservation')->createReservation($request);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof \InvalidArgumentException) {
                $statusCode = 400;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        $serializedReservation = $serializer->serialize($reservation, $_format, $serializer->getSerializationContext('details'));

        return new Response($serializedReservation, 200, $responseHeaders);
    }

    /**
     * @param Request $request
     * @param string  $_format
     * @param string  $id
     *
     * @return Response
     *
     * @Route("/reservations/{id}/ship.{_format}",
     *     name="reservation_ship",
     *     methods={"PUT"},
     *     requirements={"_format"="json"}
     * )
     */
    public function shipReservation(Request $request, $_format, $id)
    {
        $serializer = $this->get('sephora_sku.serializer');
        $responseHeaders = $this->getContentResponseHeader($request, $_format);

        try {
            $this->get('sephora_sku.reservation')->shipReservation($id);
        } catch (\Exception $ex) {
            $content = $serializer->serialize(array('msg' => $ex->getMessage()), $_format);
            $statusCode = 500;
            if ($ex instanceof EntityNotFoundException) {
                $statusCode = 404;
            } elseif ($ex instanceof \InvalidArgumentException) {
                $statusCode = 400;
            }

            return new Response($content, $statusCode, $responseHeaders);
        }

        return new Response('', 204);
    }

    /**
     * @param Request $request
     * @param string  $_format
     *
     * @return array
     */
    protected function getContentResponseHeader(Request $request, $_format)
    {
        return array('Content-Type' => $request->getMimeType($_format));
    }
}
