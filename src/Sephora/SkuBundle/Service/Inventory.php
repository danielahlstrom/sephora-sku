<?php
namespace Sephora\SkuBundle\Service;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\OptimisticLockException;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use JMS\Serializer\SerializerInterface;
use Sephora\SkuBundle\Entity\Inventory as InventoryEntity;
use Sephora\SkuBundle\Entity\Product as ProductEntity;
use Sephora\SkuBundle\Entity\Warehouse as WarehouseEntity;
use Sephora\SkuBundle\Exception\NegativeCountException;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Inventory
 * Creation date: 2018-03-30
 *
 * @package Sephora\SkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class Inventory
{
    const MODE_SET = 'set';
    const MODE_ADD = 'add';
    const MODE_REDUCE = 'reduce';

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function setDoctrine(ManagerRegistry $managerRegistry)
    {
        $this->entityManager = $managerRegistry->getManager();
    }

    /**
     * @param Request $request
     * @param string  $mode
     *
     * @return InventoryEntity
     * @throws EntityNotFoundException
     * @throws NegativeCountException
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function updateInventory(Request $request, $mode)
    {
        if (!in_array($mode, array(self::MODE_SET, self::MODE_ADD, self::MODE_REDUCE))) {
            throw new \InvalidArgumentException('Could not update inventory, invalid mode');
        }
        $inventory = $this->getInventoryFromRequest($request);

        if (0 > $inventory->getNoStock()) {
            throw new NegativeCountException('Can not '.$mode.' stock with a negative amount');
        }

        return $this->updateInventoryEntity($inventory, $mode);
    }

    /**
     * @param Request $request
     *
     * @return InventoryEntity
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    protected function getInventoryFromRequest(Request $request)
    {
        $content = $request->getContent();
        $requestFormat = $request->getRequestFormat();
        /**
         * @var InventoryEntity $inventory
         */
        try {
            $inventory = $this->serializer->deserialize($content, InventoryEntity::class, $requestFormat);
        } catch (UnsupportedFormatException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - '.$requestFormat.' format not supported');
        } catch (RuntimeException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - malformed '.$requestFormat);
        } catch (\Exception $ex) {
            throw new \Exception('Could not deserialize content');
        }

        return $inventory;
    }

    /**
     * @param InventoryEntity $inventory
     * @param string          $mode
     *
     * @return InventoryEntity
     * @throws EntityNotFoundException
     * @throws NegativeCountException
     * @throws \Exception
     */
    protected function updateInventoryEntity(InventoryEntity $inventory, $mode)
    {
        $inventoryEntity = null;
        if (strlen($inventory->getId()) > 0) {
            $inventoryEntity = $this->entityManager->find('SephoraSkuBundle:Inventory', $inventory->getId());
            if (null === $inventoryEntity) {
                throw new EntityNotFoundException('Could not find inventory with id');
            }
        } elseif ($inventory->getProduct() instanceof ProductEntity && $inventory->getWarehouse() instanceof WarehouseEntity) {
            $repo = $this->entityManager->getRepository('SephoraSkuBundle:Inventory');
            $inventoryEntity = $repo->findOneBy(array(
                'product' => $inventory->getProduct(),
                'warehouse' => $inventory->getWarehouse(),
            ));
        }
        if (!isset($inventoryEntity) || null === $inventoryEntity) {
            if ($mode === self::MODE_REDUCE) {
                throw new NegativeCountException('Inventory can not start negative');
            }

            return $this->createInventory($inventory);
        }

        try {
            $this->entityManager->lock($inventoryEntity, LockMode::OPTIMISTIC, $inventoryEntity->getVersion());
            if ($mode === self::MODE_SET) {
                $inventoryEntity->setNoStock($inventory->getNoStock());
            } elseif ($mode === self::MODE_ADD) {
                $inventoryEntity->addNoStock($inventory->getNoStock());
            } elseif ($mode === self::MODE_REDUCE) {
                $inventoryEntity->reduceNoStock($inventory->getNoStock());
            }
            if (0 > $inventoryEntity->getNoStock()) {
                throw new NegativeCountException('Inventory count can not be negative');
            }

            $this->entityManager->flush();
        } catch (OptimisticLockException $ex) {
            throw new \Exception('Inventory already updated, try again');
        }

        return $inventoryEntity;
    }

    /**
     * @param InventoryEntity $inventory
     *
     * @return InventoryEntity
     * @throws OptimisticLockException
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    protected function createInventory(InventoryEntity $inventory)
    {
        $product = $this->entityManager->find('SephoraSkuBundle:Product', $inventory->getProduct()->getId());
        $warehouse = $this->entityManager->find('SephoraSkuBundle:Warehouse', $inventory->getWarehouse()->getId());
        if (null === $product || null === $warehouse) {
            throw new \InvalidArgumentException('Invalid product and/or warehouse ids provided');
        }
        $inventoryEntity = new InventoryEntity();
        $inventoryEntity->setNoStock($inventory->getNoStock());
        $inventoryEntity->setProduct($product);
        $inventoryEntity->setWarehouse($warehouse);
        $this->entityManager->persist($inventoryEntity);
        $this->entityManager->lock($inventoryEntity, LockMode::OPTIMISTIC);
        $this->entityManager->flush();

        return $inventoryEntity;
    }
}
