<?php
namespace Sephora\SkuBundle\Service;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\OptimisticLockException;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use JMS\Serializer\SerializerInterface;
use Sephora\SkuBundle\Entity\Customer as CustomerEntity;
use Sephora\SkuBundle\Entity\Inventory as InventoryEntity;
use Sephora\SkuBundle\Entity\Reservation as ReservationEntity;
use Sephora\SkuBundle\Exception\NegativeCountException;
use Sephora\SkuBundle\Exception\TooManyReservedException;
use Sephora\SkuBundle\Model\DbFilters;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Reservation
 * Creation date: 2018-03-30
 *
 * @package SephoraSkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class Reservation
{
    const CSV_SEPARATOR = ';';

    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function setDoctrine(ManagerRegistry $managerRegistry)
    {
        $this->entityManager = $managerRegistry->getManager();
    }

    /**
     * @param Request $request
     *
     * @return ReservationEntity[]
     */
    public function getReservations(Request $request)
    {
        $filters = $this->getFiltersFromRequest($request);
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Reservation');

        return $repo->getReservations($filters->getCriteria(), $filters->getOrderBy(), $filters->getLimit(), $filters->getOffset());
    }

    /**
     * @param Request $request
     *
     * @return string
     */
    public function getReservationsAsCsv(Request $request)
    {
        $filters = $this->getFiltersFromRequest($request);
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Reservation');

        $reservations = $repo->getReservations($filters->getCriteria(), $filters->getOrderBy(), $filters->getLimit(), $filters->getOffset());

        return $this->reservationsToCsv($reservations);
    }

    /**
     * @param string $id
     *
     * @return ReservationEntity
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function getReservation($id)
    {
        $reservation = $this->entityManager->find('SephoraSkuBundle:Reservation', $id);
        if (null === $reservation) {
            throw new EntityNotFoundException('Could not find a reservation with that id');
        }

        return $reservation;
    }

    /**
     * @param Request $request
     *
     * @return ReservationEntity
     * @throws TooManyReservedException
     * @throws \InvalidArgumentException
     * @throws \Exception
     */
    public function createReservation(Request $request)
    {
        $content = $request->getContent();
        $requestFormat = $request->getRequestFormat();
        /**
         * @var ReservationEntity $reservation
         */
        try {
            $reservation = $this->serializer->deserialize($content, ReservationEntity::class, $requestFormat);
        } catch (UnsupportedFormatException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - '.$requestFormat.' format not supported');
        } catch (RuntimeException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - malformed '.$requestFormat);
        } catch (\Exception $ex) {
            throw new \Exception('Could not deserialize content');
        }

        if (!($reservation->getCustomer() instanceof CustomerEntity) || !($reservation->getInventory() instanceof InventoryEntity)) {
            throw new \InvalidArgumentException('Invalid customer and/or inventory');
        }

        if (!is_int($reservation->getNoItems())) {
            throw new \InvalidArgumentException('Number of items to reserve must be an integer');
        } elseif (1 > $reservation->getNoItems()) {
            throw new NegativeCountException('Can not make a reservation for no items or reserve a negative amount of items');
        }

        $customer = $this->entityManager->find('SephoraSkuBundle:Customer', $reservation->getCustomer()->getId());
        $inventory = $this->entityManager->find('SephoraSkuBundle:Inventory', $reservation->getInventory()->getId());

        if (null === $customer || null === $inventory) {
            throw new \InvalidArgumentException('Could not find customer or inventory');
        }

        try {
            $reservation->setCustomer($customer);
            $reservation->setInventory($inventory);

            $this->entityManager->lock($inventory, LockMode::OPTIMISTIC, $inventory->getVersion());
            $inventory->addNoReserved($reservation->getNoItems());
            if ($inventory->getNoReserved() > $inventory->getNoStock()) {
                throw new TooManyReservedException('Can not reserve more than stock');
            }

            $this->entityManager->persist($reservation);
            $this->entityManager->flush();
        } catch (OptimisticLockException $ex) {
            throw new \Exception('Inventory already updated, try again');
        }

        return $reservation;
    }

    /**
     * @param string $id
     *
     * @throws EntityNotFoundException
     * @throws NegativeCountException
     * @throws \Exception
     */
    public function shipReservation($id)
    {
        $reservation = $this->entityManager->find('SephoraSkuBundle:Reservation', $id);
        if (null === $reservation) {
            throw new EntityNotFoundException('Could not find a reservation with that id');
        }

        try {
            $inventory = $reservation->getInventory();
            $this->entityManager->lock($inventory, LockMode::OPTIMISTIC, $inventory->getVersion());

            $inventory->reduceNoReserved($reservation->getNoItems());
            $inventory->reduceNoStock($reservation->getNoItems());
            if (0 > $inventory->getNoStock() || 0 > $inventory->getNoReserved()) {
                throw new NegativeCountException('Number in stock and/or number of reserved can not be negative');
            }

            $this->entityManager->remove($reservation);
            $this->entityManager->flush();
        } catch (OptimisticLockException $ex) {
            throw new \Exception('Inventory already updated, try again');
        }
    }

    /**
     * @param Request $request
     *
     * @return DbFilters
     */
    protected function getFiltersFromRequest(Request $request)
    {
        $filters = new DbFilters();
        if ($request->query->has('page')) {
            $filters->setPage($request->query->get('page'));
        }
        if ($request->query->has('page_size')) {
            $filters->setPageSize($request->query->get('page_size'));
        }

        return $filters;
    }

    /**
     * @param ReservationEntity[] $reservations
     *
     * @return string
     */
    protected function reservationsToCsv(array $reservations)
    {
        $lines = array();
        foreach ($reservations as $reservation) {
            $lines[] = $this->reservationToCsvLine($reservation);
        }

        return implode(PHP_EOL, $lines);
    }

    /**
     * @param ReservationEntity $reservation
     *
     * @return string
     */
    protected function reservationToCsvLine(ReservationEntity $reservation)
    {
        $line = array(
            $reservation->getInventory()->getWarehouse()->getId(),
            $reservation->getInventory()->getWarehouse()->getName(),
            $reservation->getInventory()->getProduct()->getId(),
            $reservation->getInventory()->getProduct()->getName(),
            $reservation->getCustomer()->getId(),
            $reservation->getCustomer()->getName(),
            $reservation->getNoItems(),
        );

        return implode(self::CSV_SEPARATOR, $line);
    }
}
