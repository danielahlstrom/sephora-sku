<?php
namespace Sephora\SkuBundle\Service;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use JMS\Serializer\SerializerInterface;
use Sephora\SkuBundle\Entity\Customer as CustomerEntity;
use Sephora\SkuBundle\Entity\Reservation as ReservationEntity;
use Sephora\SkuBundle\Model\DbFilters;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Customer
 * Creation date: 2018-03-30
 *
 * @package Sephora\SkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class Customer
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function setDoctrine(ManagerRegistry $managerRegistry)
    {
        $this->entityManager = $managerRegistry->getManager();
    }

    /**
     * @param Request $request
     *
     * @return CustomerEntity[]
     */
    public function getCustomers(Request $request)
    {
        $filters = $this->getFiltersFromRequest($request);
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Customer');

        return $repo->findBy($filters->getCriteria(), $filters->getOrderBy(), $filters->getLimit(), $filters->getOffset());
    }

    /**
     * @param string $id
     *
     * @return null|CustomerEntity
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function getCustomer($id)
    {
        $customer = $this->entityManager->find('SephoraSkuBundle:Customer', $id);
        if (null === $customer) {
            throw new EntityNotFoundException('Could not find customer with that id');
        }

        return $customer;
    }

    /**
     * @param string $id
     *
     * @return ReservationEntity[]
     * @throws \Exception
     */
    public function getReservations($id)
    {
        $customer = $this->entityManager->find('SephoraSkuBundle:Customer', $id);
        if (null === $customer) {
            throw new EntityNotFoundException('Could not find customer with that id');
        }
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Reservation');

        return $repo->getReservationsForCustomer($id);
    }

    /**
     * @param Request $request
     *
     * @return CustomerEntity
     * @throws \Exception
     */
    public function createCustomer(Request $request)
    {
        $content = $request->getContent();
        $requestFormat = $request->getRequestFormat();
        /**
         * @var CustomerEntity $customer
         */
        try {
            $customer = $this->serializer->deserialize($content, CustomerEntity::class, $requestFormat);
        } catch (UnsupportedFormatException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - '.$requestFormat.' format not supported');
        } catch (RuntimeException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - malformed '.$requestFormat);
        } catch (\Exception $ex) {
            throw new \Exception('Could not deserialize content');
        }

        $this->entityManager->persist($customer);
        $this->entityManager->flush();

        return $customer;
    }

    /**
     * @param Request $request
     *
     * @return DbFilters
     */
    protected function getFiltersFromRequest(Request $request)
    {
        $filters = new DbFilters();
        $filters->setOrderBy(array(
            'name' => 'ASC',
        ));
        if ($request->query->has('page')) {
            $filters->setPage($request->query->get('page'));
        }
        if ($request->query->has('page_size')) {
            $filters->setPageSize($request->query->get('page_size'));
        }

        return $filters;
    }
}
