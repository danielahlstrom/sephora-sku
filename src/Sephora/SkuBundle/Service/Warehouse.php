<?php
namespace Sephora\SkuBundle\Service;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use JMS\Serializer\SerializerInterface;
use Sephora\SkuBundle\Entity\Inventory as InventoryEntity;
use Sephora\SkuBundle\Entity\Reservation as ReservationEntity;
use Sephora\SkuBundle\Entity\Warehouse as WarehouseEntity;
use Sephora\SkuBundle\Model\DbFilters;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Warehouse
 * Creation date: 2018-03-30
 *
 * @package SkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class Warehouse
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function setDoctrine(ManagerRegistry $managerRegistry)
    {
        $this->entityManager = $managerRegistry->getManager();
    }

    /**
     * @param Request $request
     *
     * @return WarehouseEntity[]
     */
    public function getWarehouses(Request $request)
    {
        $filters = $this->getFiltersFromRequest($request);
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Warehouse');

        return $repo->findBy($filters->getCriteria(), $filters->getOrderBy(), $filters->getLimit(), $filters->getOffset());
    }

    /**
     * @param string $id
     *
     * @return InventoryEntity[]
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function getInventories($id)
    {
        $warehouse = $this->entityManager->find('SephoraSkuBundle:Warehouse', $id);
        if (null === $warehouse) {
            throw new EntityNotFoundException('Could not find warehouse with that id');
        }
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Inventory');

        return $repo->getInventoriesForWarehouse($id);
    }

    /**
     * @param string $id
     *
     * @return ReservationEntity[]
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function getReservations($id)
    {
        $warehouse = $this->entityManager->find('SephoraSkuBundle:Warehouse', $id);
        if (null === $warehouse) {
            throw new EntityNotFoundException('Could not find warehouse with that id');
        }
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Reservation');

        return $repo->getReservationsForWarehouse($id);
    }

    /**
     * @param Request $request
     *
     * @return WarehouseEntity
     * @throws \Exception
     */
    public function createWarehouse(Request $request)
    {
        $content = $request->getContent();
        $requestFormat = $request->getRequestFormat();
        /**
         * @var WarehouseEntity $warehouse
         */
        try {
            $warehouse = $this->serializer->deserialize($content, WarehouseEntity::class, $requestFormat);
        } catch (UnsupportedFormatException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - '.$requestFormat.' format not supported');
        } catch (RuntimeException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - malformed '.$requestFormat);
        } catch (\Exception $ex) {
            throw new \Exception('Could not deserialize content');
        }

        $this->entityManager->persist($warehouse);
        $this->entityManager->flush();

        return $warehouse;
    }

    /**
     * @param Request $request
     *
     * @return DbFilters
     */
    protected function getFiltersFromRequest(Request $request)
    {
        $filters = new DbFilters();
        $filters->setOrderBy(array(
            'name' => 'ASC',
        ));
        if ($request->query->has('page')) {
            $filters->setPage($request->query->get('page'));
        }
        if ($request->query->has('page_size')) {
            $filters->setPageSize($request->query->get('page_size'));
        }

        return $filters;
    }
}
