<?php
namespace Sephora\SkuBundle\Service;

use JMS\Serializer\DeserializationContext;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer as JMSSerializer;
use JMS\Serializer\SerializerBuilder;
use JMS\Serializer\SerializerInterface;

/**
 * Class Serializer
 * Creation date: 2018-03-30
 *
 * @package SephoraSkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class Serializer implements SerializerInterface
{
    /**
     * @var JMSSerializer
     */
    private $serializer;

    /**
     * Serializer constructor.
     *
     * @param string $kernelCacheDir
     * @param bool   $debug
     */
    public function __construct($kernelCacheDir, $debug)
    {
        $cacheDir = $kernelCacheDir.'/jms_serializer';
        $this->serializer = SerializerBuilder::create()->setCacheDir($cacheDir)->setDebug($debug)->build();
    }

    /**
     * @param mixed                     $data
     * @param string                    $format
     * @param SerializationContext|null $context
     *
     * @return string
     */
    public function serialize($data, $format, SerializationContext $context = null)
    {
        return $this->serializer->serialize($data, $format, $context);
    }

    /**
     * @param string                      $data
     * @param string                      $type
     * @param string                      $format
     * @param DeserializationContext|null $context
     *
     * @return mixed
     */
    public function deserialize($data, $type, $format, DeserializationContext $context = null)
    {
        return $this->serializer->deserialize($data, $type, $format, $context);
    }

    /**
     * @param array|string $groups
     *
     * @return SerializationContext
     */
    public function getSerializationContext($groups)
    {
        $serializationContext = SerializationContext::create();
        $serializationContext->setGroups($groups);

        return $serializationContext;
    }

    /**
     * @param array|string $groups
     *
     * @return DeserializationContext
     */
    public function getDeserializationContext($groups)
    {
        $deserializationContext = DeserializationContext::create();
        $deserializationContext->setGroups($groups);

        return $deserializationContext;
    }
}
