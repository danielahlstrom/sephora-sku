<?php
namespace Sephora\SkuBundle\Service;

use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityNotFoundException;
use JMS\Serializer\Exception\RuntimeException;
use JMS\Serializer\Exception\UnsupportedFormatException;
use JMS\Serializer\SerializerInterface;
use Sephora\SkuBundle\Entity\Inventory as InventoryEntity;
use Sephora\SkuBundle\Entity\Product as ProductEntity;
use Sephora\SkuBundle\Entity\Reservation as ReservationEntity;
use Sephora\SkuBundle\Model\DbFilters;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Product
 * Creation date: 2018-03-27
 *
 * @package SephoraSkuBundle\Service
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class Product
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param SerializerInterface $serializer
     */
    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @param ManagerRegistry $managerRegistry
     */
    public function setDoctrine(ManagerRegistry $managerRegistry)
    {
        $this->entityManager = $managerRegistry->getManager();
    }

    /**
     * @param Request $request
     *
     * @return ProductEntity[]
     */
    public function getProducts(Request $request)
    {
        $filters = $this->getFiltersFromRequest($request);
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Product');

        return $repo->findBy($filters->getCriteria(), $filters->getOrderBy(), $filters->getLimit(), $filters->getOffset());
    }

    /**
     * @param string $id
     *
     * @return ProductEntity
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function getProduct($id)
    {
        $product = $this->entityManager->find('SephoraSkuBundle:Product', $id);
        if (null === $product) {
            throw new EntityNotFoundException('Could not find product with that id');
        }

        return $product;
    }

    /**
     * @param string $id
     *
     * @return InventoryEntity[]
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function getInventories($id)
    {
        $product = $this->entityManager->find('SephoraSkuBundle:Product', $id);
        if (null === $product) {
            throw new EntityNotFoundException('Could not find product with that id');
        }
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Inventory');

        return $repo->getInventoriesForProduct($id);
    }

    /**
     * @param $id
     *
     * @return ReservationEntity[]
     * @throws EntityNotFoundException
     * @throws \Exception
     */
    public function getReservations($id)
    {
        $product = $this->entityManager->find('SephoraSkuBundle:Product', $id);
        if (null === $product) {
            throw new EntityNotFoundException('Could not find product with that id');
        }
        $repo = $this->entityManager->getRepository('SephoraSkuBundle:Reservation');

        return $repo->getReservationsForProduct($id);
    }

    /**
     * @param Request $request
     *
     * @return ProductEntity
     * @throws \Exception
     */
    public function createProduct(Request $request)
    {
        $content = $request->getContent();
        $requestFormat = $request->getRequestFormat();
        /**
         * @var ProductEntity $product
         */
        try {
            $product = $this->serializer->deserialize($content, ProductEntity::class, $requestFormat);
        } catch (UnsupportedFormatException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - '.$requestFormat.' format not supported');
        } catch (RuntimeException $ex) {
            throw new \InvalidArgumentException('Could not deserialize content - malformed '.$requestFormat);
        } catch (\Exception $ex) {
            throw new \Exception('Could not deserialize content');
        }

        $this->entityManager->persist($product);
        $this->entityManager->flush();

        return $product;
    }

    /**
     * @param Request $request
     *
     * @return DbFilters
     */
    protected function getFiltersFromRequest(Request $request)
    {
        $filters = new DbFilters();
        $filters->setOrderBy(array(
            'name' => 'ASC',
        ));
        if ($request->query->has('page')) {
            $filters->setPage($request->query->get('page'));
        }
        if ($request->query->has('page_size')) {
            $filters->setPageSize($request->query->get('page_size'));
        }

        return $filters;
    }
}
