<?php
namespace Sephora\SkuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMSSer;
use Ramsey\Uuid\Uuid;

/**
 * Class Customer
 * Creation date: 2018-03-26
 *
 * @package Sephora\SkuBundle\Entity
 * @author  Daniel Ahlström <danahl@gmail.com>
 *
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Customer
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=64)
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_prod"})
     * @JMSSer\Type("string")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=256)
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_prod"})
     * @JMSSer\Type("string")
     */
    protected $name;

    /**
     * @var Reservation[]
     *
     * @ORM\OneToMany(targetEntity="Reservation", mappedBy="customer")
     *
     * @JMSSer\Groups({"extended"})
     * @JMSSer\Type("array<Sephora\SkuBundle\Entity\Reservation>")
     */
    protected $reservations = array();

    /**
     * Generate random id
     *
     * @ORM\PrePersist
     */
    public function generateId()
    {
        if (!empty($this->id)) {
            return;
        }

        $this->id = Uuid::uuid4()->toString();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return Reservation[]
     */
    public function getReservations()
    {
        return $this->reservations;
    }

    /**
     * @param Reservation $reservation
     */
    public function addReservation($reservation)
    {
        $this->reservations[] = $reservation;
    }
}
