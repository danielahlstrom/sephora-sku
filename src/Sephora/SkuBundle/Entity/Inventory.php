<?php
namespace Sephora\SkuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMSSer;
use Ramsey\Uuid\Uuid;

/**
 * Class Inventory
 * Creation date: 2018-03-26
 *
 * @package Sephora\SkuBundle\Entity
 * @author  Daniel Ahlström <danahl@gmail.com>
 *
 * @ORM\Entity(repositoryClass="Sephora\SkuBundle\Repository\InventoryRepository")
 * @ORM\Table(
 *     uniqueConstraints={@ORM\UniqueConstraint(name="warehouse_product_uidx", columns={"warehouse_id", "product_id"})}
 * )
 * @ORM\HasLifecycleCallbacks
 */
class Inventory
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=64)
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_wh", "list_res_for_prod"})
     * @JMSSer\Type("string")
     */
    protected $id;

    /**
     * @var Warehouse
     *
     * @ORM\ManyToOne(targetEntity="Warehouse", fetch="EAGER")
     * @ORM\JoinColumn(name="warehouse_id", referencedColumnName="id")
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_prod"})
     * @JMSSer\Type("Sephora\SkuBundle\Entity\Warehouse")
     */
    protected $warehouse;

    /**
     * @var Product
     *
     * @ORM\ManyToOne(targetEntity="Product", fetch="EAGER")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_wh"})
     * @JMSSer\Type("Sephora\SkuBundle\Entity\Product")
     */
    protected $product;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_wh", "list_res_for_prod"})
     * @JMSSer\Type("integer")
     */
    protected $noStock = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_wh", "list_res_for_prod"})
     * @JMSSer\Type("integer")
     */
    protected $noReserved = 0;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer")
     * @ORM\Version
     *
     * @JMSSer\Groups({"details"})
     * @JMSSer\Type("integer")
     */
    protected $version = 0;

    /**
     * Generate random id
     *
     * @ORM\PrePersist
     */
    public function generateId()
    {
        if (!empty($this->id)) {
            return;
        }

        $this->id = Uuid::uuid4()->toString();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Warehouse
     */
    public function getWarehouse()
    {
        return $this->warehouse;
    }

    /**
     * @param Warehouse $warehouse
     */
    public function setWarehouse($warehouse)
    {
        $this->warehouse = $warehouse;
    }

    /**
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product $product
     */
    public function setProduct($product)
    {
        $this->product = $product;
    }

    /**
     * @return int
     */
    public function getNoStock()
    {
        return $this->noStock;
    }

    /**
     * @param int $noStock
     */
    public function setNoStock($noStock)
    {
        $this->noStock = $noStock;
    }

    /**
     * @param int $noStock
     */
    public function addNoStock($noStock)
    {
        $this->noStock += $noStock;
    }

    /**
     * @param int $noStock
     */
    public function reduceNoStock($noStock)
    {
        $this->noStock -= $noStock;
    }

    /**
     * @return int
     */
    public function getNoReserved()
    {
        return $this->noReserved;
    }

    /**
     * @param int $noReserved
     */
    public function setNoReserved($noReserved)
    {
        $this->noReserved = $noReserved;
    }

    /**
     * @param int $noReserved
     */
    public function addNoReserved($noReserved)
    {
        $this->noReserved += $noReserved;
    }

    /**
     * @param int $noReserved
     */
    public function reduceNoReserved($noReserved)
    {
        $this->noReserved -= $noReserved;
    }

    /**
     * @return int
     */
    public function getVersion()
    {
        return $this->version;
    }
}
