<?php
namespace Sephora\SkuBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMSSer;
use Ramsey\Uuid\Uuid;

/**
 * Class Reservation
 * Creation date: 2018-03-26
 *
 * @package Sephora\SkuBundle\Entity
 * @author  Daniel Ahlström <danahl@gmail.com>
 *
 * @ORM\Entity(repositoryClass="Sephora\SkuBundle\Repository\ReservationRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Reservation
{
    /**
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(type="string", length=64)
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_wh", "list_res_for_prod"})
     * @JMSSer\Type("string")
     */
    protected $id;

    /**
     * @var Customer
     *
     * @ORM\ManyToOne(targetEntity="Customer", inversedBy="reservations", fetch="EAGER")
     * @ORM\JoinColumn(name="customer_id", referencedColumnName="id")
     *
     * @JMSSer\Groups({"details", "list_res_for_prod"})
     * @JMSSer\Type("Sephora\SkuBundle\Entity\Customer")
     */
    protected $customer;

    /**
     * @var Inventory
     *
     * @ORM\ManyToOne(targetEntity="Inventory", fetch="EAGER")
     * @ORM\JoinColumn(name="inventory_id", referencedColumnName="id")
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_wh", "list_res_for_prod"})
     * @JMSSer\Type("Sephora\SkuBundle\Entity\Inventory")
     */
    protected $inventory;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     *
     * @JMSSer\Groups({"list", "details", "list_res_for_wh", "list_res_for_prod"})
     * @JMSSer\Type("integer")
     */
    protected $noItems;

    /**
     * Generate random id
     *
     * @ORM\PrePersist
     */
    public function generateId()
    {
        if (!empty($this->id)) {
            return;
        }

        $this->id = Uuid::uuid4()->toString();
    }

    /**
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Customer
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * @param Customer $customer
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;
    }

    /**
     * @return Inventory
     */
    public function getInventory()
    {
        return $this->inventory;
    }

    /**
     * @param Inventory $inventory
     */
    public function setInventory($inventory)
    {
        $this->inventory = $inventory;
    }

    /**
     * @return int
     */
    public function getNoItems()
    {
        return $this->noItems;
    }

    /**
     * @param int $noItems
     */
    public function setNoItems($noItems)
    {
        $this->noItems = $noItems;
    }
}
