<?php
namespace Sephora\SkuBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class SephoraSkuBundle
 *
 * @package Sephora\SkuBundle
 */
class SephoraSkuBundle extends Bundle
{
}
