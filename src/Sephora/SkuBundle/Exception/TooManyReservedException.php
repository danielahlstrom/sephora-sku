<?php
namespace Sephora\SkuBundle\Exception;

/**
 * Class TooManyReservedException
 * Creation date: 2018-03-31
 *
 * @package Sephora\SkuBundle\Exception
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class TooManyReservedException extends \InvalidArgumentException
{
}
