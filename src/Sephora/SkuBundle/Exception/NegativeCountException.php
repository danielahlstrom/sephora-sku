<?php
namespace Sephora\SkuBundle\Exception;

/**
 * Class NegativeCountException
 * Creation date: 2018-03-30
 *
 * @package Sephora\SkuBundle\Exception
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class NegativeCountException extends \InvalidArgumentException
{
}
