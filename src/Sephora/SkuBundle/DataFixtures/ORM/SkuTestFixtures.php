<?php
namespace Sephora\SkuBundle\DataFixtures\ORM;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Sephora\SkuBundle\Entity\Customer;
use Sephora\SkuBundle\Entity\Inventory;
use Sephora\SkuBundle\Entity\Product;
use Sephora\SkuBundle\Entity\Reservation;
use Sephora\SkuBundle\Entity\Warehouse;

/**
 * Class SkuTestFixtures
 * Creation date: 2018-04-02
 *
 * @package Sephora\SkuBundle\DataFixtures\ORM
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class SkuTestFixtures extends Fixture
{
    /**
     * @param ObjectManager|EntityManager $manager
     */
    public function load(ObjectManager $manager)
    {
        // Using serializer to set predictable ids
        $serializer = $this->container->get('sephora_sku.serializer');

        /*
         * Create warehouses
         */
        // Create warehouses which will have inventory
        $warehouseSing = $serializer->deserialize('{"id":"sing","name":"Singapore"}', Warehouse::class, 'json');
        $manager->persist($warehouseSing);

        $warehouseThai = $serializer->deserialize('{"id":"thai","name":"Thailand"}', Warehouse::class, 'json');
        $manager->persist($warehouseThai);

        // Create random warehouses for pagination tests
        for ($i = 0; $i < 10; $i += 1) {
            $warehouse = new Warehouse();
            $warehouse->setName('Warehouse '.$i);
            $manager->persist($warehouse);
        }

        /*
         * Create customers
         */
        // Create customers that will make reservations
        $customerLuke = $serializer->deserialize('{"id":"luke","name":"Luke"}', Customer::class, 'json');
        $manager->persist($customerLuke);

        $customerLeia = $serializer->deserialize('{"id":"leia","name":"Leia"}', Customer::class, 'json');
        $manager->persist($customerLeia);

        // Create random customers for pagination tests
        for ($i = 0; $i < 10; $i += 1) {
            $customer = new Customer();
            $customer->setName('Customer '.$i);
            $manager->persist($customer);
        }

        /*
         * Create products
         */
        // Create products that will be used in inventories and reservations
        $productPencil = $serializer->deserialize('{"id":"pencil","name":"Pencil"}', Product::class, 'json');
        $manager->persist($productPencil);

        $productPaper = $serializer->deserialize('{"id":"paper","name":"Paper"}', Product::class, 'json');
        $manager->persist($productPaper);

        $productRuler = $serializer->deserialize('{"id":"ruler","name":"Ruler"}', Product::class, 'json');
        $manager->persist($productRuler);

        $productCalendar = $serializer->deserialize('{"id":"calendar","name":"Calendar"}', Product::class, 'json');
        $manager->persist($productCalendar);

        // Create random products for pagination tests
        for ($i = 0; $i < 10; $i += 1) {
            $product = new Product();
            $product->setName('Product '.$i);
            $manager->persist($product);
        }

        /*
         * Create inventories
         */
        // Singapore has pencils, papers and rulers
        $inventorySingPencil = $serializer->deserialize('{"id":"sing-pencil"}', Inventory::class, 'json');
        $inventorySingPencil->setWarehouse($warehouseSing);
        $inventorySingPencil->setProduct($productPencil);
        $inventorySingPencil->setNoStock(10);
        $manager->persist($inventorySingPencil);

        $inventorySingPaper = $serializer->deserialize('{"id":"sing-paper"}', Inventory::class, 'json');
        $inventorySingPaper->setWarehouse($warehouseSing);
        $inventorySingPaper->setProduct($productPaper);
        $inventorySingPaper->setNoStock(100);
        $manager->persist($inventorySingPaper);

        $inventorySingRuler = $serializer->deserialize('{"id":"sing-ruler"}', Inventory::class, 'json');
        $inventorySingRuler->setWarehouse($warehouseSing);
        $inventorySingRuler->setProduct($productRuler);
        $inventorySingRuler->setNoStock(5);
        $manager->persist($inventorySingRuler);

        // Thailand has pencils and papers
        $inventoryThaiPencil = $serializer->deserialize('{"id":"thai-pencil"}', Inventory::class, 'json');
        $inventoryThaiPencil->setWarehouse($warehouseThai);
        $inventoryThaiPencil->setProduct($productPencil);
        $inventoryThaiPencil->setNoStock(1);
        $manager->persist($inventoryThaiPencil);

        $inventoryThaiPaper = $serializer->deserialize('{"id":"thai-paper"}', Inventory::class, 'json');
        $inventoryThaiPaper->setWarehouse($warehouseThai);
        $inventoryThaiPaper->setProduct($productPaper);
        $inventoryThaiPaper->setNoStock(50);
        $manager->persist($inventoryThaiPaper);

        /*
         * Create reservations
         */
        // Luke reserves pencils from Singapore
        $reservationLukePencil = $serializer->deserialize('{"id":"luke-sing-pencil"}', Reservation::class, 'json');
        $reservationLukePencil->setCustomer($customerLuke);
        $reservationLukePencil->setInventory($inventorySingPencil);
        $reservationLukePencil->setNoItems(3);
        $inventorySingPencil->addNoReserved(3);
        $manager->persist($reservationLukePencil);

        // Luke reserves papers from Singapore
        $reservationLukePaper1 = $serializer->deserialize('{"id":"luke-sing-paper"}', Reservation::class, 'json');
        $reservationLukePaper1->setCustomer($customerLuke);
        $reservationLukePaper1->setInventory($inventorySingPaper);
        $reservationLukePaper1->setNoItems(30);
        $inventorySingPaper->addNoReserved(30);
        $manager->persist($reservationLukePaper1);

        // Luke reserves papers from Thailand
        $reservationLukePaper2 = $serializer->deserialize('{"id":"luke-thai-paper"}', Reservation::class, 'json');
        $reservationLukePaper2->setCustomer($customerLuke);
        $reservationLukePaper2->setInventory($inventoryThaiPaper);
        $reservationLukePaper2->setNoItems(20);
        $inventoryThaiPaper->addNoReserved(20);
        $manager->persist($reservationLukePaper2);

        // Luke reserves ruler from Singapore
        $reservationLukeRuler = $serializer->deserialize('{"id":"luke-sing-ruler"}', Reservation::class, 'json');
        $reservationLukeRuler->setCustomer($customerLuke);
        $reservationLukeRuler->setInventory($inventorySingRuler);
        $reservationLukeRuler->setNoItems(1);
        $inventorySingRuler->addNoReserved(1);
        $manager->persist($reservationLukeRuler);

        // Leia reserves pencil from Thailand
        $reservationLeiaPencil1 = $serializer->deserialize('{"id":"leia-thai-pencil"}', Reservation::class, 'json');
        $reservationLeiaPencil1->setCustomer($customerLeia);
        $reservationLeiaPencil1->setInventory($inventoryThaiPencil);
        $reservationLeiaPencil1->setNoItems(1);
        $inventoryThaiPencil->addNoReserved(1);
        $manager->persist($reservationLeiaPencil1);

        // Leia reserves pencils from Singapore
        $reservationLeiaPencil2 = $serializer->deserialize('{"id":"leia-sing-pencil"}', Reservation::class, 'json');
        $reservationLeiaPencil2->setCustomer($customerLeia);
        $reservationLeiaPencil2->setInventory($inventorySingPencil);
        $reservationLeiaPencil2->setNoItems(2);
        $inventorySingPencil->addNoReserved(2);
        $manager->persist($reservationLeiaPencil2);

        // Leia reserves papers from Thailand
        $reservationLeiaPaper = $serializer->deserialize('{"id":"leia-thai-paper"}', Reservation::class, 'json');
        $reservationLeiaPaper->setCustomer($customerLeia);
        $reservationLeiaPaper->setInventory($inventoryThaiPaper);
        $reservationLeiaPaper->setNoItems(25);
        $inventoryThaiPaper->addNoReserved(25);
        $manager->persist($reservationLeiaPaper);

        $manager->flush();
    }
}
