<?php
namespace Sephora\SkuBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Sephora\SkuBundle\Entity\Reservation;

/**
 * Class ReservationRepository
 * Creation date: 2018-03-31
 *
 * @package Sephora\SkuBundle\Repository
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class ReservationRepository extends EntityRepository
{
    /**
     * @param array $criteria
     * @param array $orderBy
     * @param int   $limit
     * @param int   $offset
     *
     * @return Reservation[]
     */
    public function getReservations(array $criteria, array $orderBy, $limit, $offset)
    {
        $qb = $this->getBasicListQueryBuilder();

        $qb->orderBy('w.name', 'ASC');
        $qb->addOrderBy('p.name', 'ASC');
        $qb->addOrderBy('c.name', 'ASC');

        $qb->setMaxResults($limit);
        $qb->setFirstResult($offset);

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $customerId
     *
     * @return Reservation[]
     */
    public function getReservationsForCustomer($customerId)
    {
        $qb = $this->getBasicListQueryBuilder();

        $qb->where($qb->expr()->eq('c.id', ':customerId'));
        $qb->setParameter('customerId', $customerId);

        $qb->orderBy('p.name', 'ASC');
        $qb->addOrderBy('w.name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $productId
     *
     * @return Reservation[]
     */
    public function getReservationsForProduct($productId)
    {
        $qb = $this->getBasicListQueryBuilder();

        $qb->where($qb->expr()->eq('p.id', ':productId'));
        $qb->setParameter('productId', $productId);

        $qb->orderBy('w.name', 'ASC');
        $qb->addOrderBy('c.name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @param string $warehouseId
     *
     * @return Reservation[]
     */
    public function getReservationsForWarehouse($warehouseId)
    {
        $qb = $this->getBasicListQueryBuilder();

        $qb->where($qb->expr()->eq('w.id', ':warehouseId'));
        $qb->setParameter('warehouseId', $warehouseId);

        $qb->orderBy('p.name', 'ASC');
        $qb->addOrderBy('c.name', 'ASC');

        return $qb->getQuery()->getResult();
    }

    /**
     * @return QueryBuilder
     */
    protected function getBasicListQueryBuilder()
    {
        $qb = $this->createQueryBuilder('r');
        $qb->select('r');
        $qb->innerJoin('r.customer', 'c');
        $qb->innerJoin('r.inventory', 'i');
        $qb->innerJoin('i.product', 'p');
        $qb->innerJoin('i.warehouse', 'w');

        return $qb;
    }
}
