<?php
namespace Sephora\SkuBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Sephora\SkuBundle\Entity\Inventory;

/**
 * Class InventoryRepository
 * Creation date: 2018-03-30
 *
 * @package Sephora\SkuBundle\Repository
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class InventoryRepository extends EntityRepository
{
    /**
     * @param string $productId
     *
     * @return Inventory[]
     */
    public function getInventoriesForProduct($productId)
    {
        $qb = $this->createQueryBuilder('i');
        $qb->select('i');
        $qb->leftJoin('i.warehouse', 'w');
        $qb->leftJoin('i.product', 'p');

        $qb->where($qb->expr()->eq('p.id', ':productId'));
        $qb->setParameter('productId', $productId);

        $qb->orderBy('w.name', 'ASC');

        $inventories = $qb->getQuery()->getResult();

        return $inventories;
    }

    /**
     * @param string $warehouseId
     *
     * @return Inventory[]
     */
    public function getInventoriesForWarehouse($warehouseId)
    {
        $qb = $this->createQueryBuilder('i');
        $qb->select('i');
        $qb->leftJoin('i.warehouse', 'w');
        $qb->leftJoin('i.product', 'p');

        $qb->where($qb->expr()->eq('w.id', ':warehouseId'));
        $qb->setParameter('warehouseId', $warehouseId);

        $qb->orderBy('p.name', 'ASC');

        $inventories = $qb->getQuery()->getResult();

        return $inventories;
    }
}
