<?php
namespace Sephora\SkuBundle\Model;

/**
 * Class DbFilters
 * Creation date: 2018-03-30
 *
 * @package Sephora\SkuBundle\Model
 * @author  Daniel Ahlström <danahl@gmail.com>
 */
class DbFilters
{
    /**
     * @var array
     */
    protected $criteria = array();

    /**
     * @var array
     */
    protected $orderBy = array();

    /**
     * @var int
     */
    protected $page = 0;

    /**
     * @var int
     */
    protected $pageSize = 100;

    /**
     * @return array
     */
    public function getCriteria()
    {
        return $this->criteria;
    }

    /**
     * @param array $criteria
     */
    public function setCriteria($criteria)
    {
        $this->criteria = $criteria;
    }

    /**
     * @return array
     */
    public function getOrderBy()
    {
        return $this->orderBy;
    }

    /**
     * @param array $orderBy
     */
    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;
    }

    /**
     * @return int
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage($page)
    {
        $this->page = $page;
    }

    /**
     * @return int
     */
    public function getPageSize()
    {
        return $this->pageSize;
    }

    /**
     * @param int $pageSize
     */
    public function setPageSize($pageSize)
    {
        $this->pageSize = $pageSize;
    }

    /**
     * @return int
     */
    public function getLimit()
    {
        return $this->pageSize;
    }

    /**
     * @return int
     */
    public function getOffset()
    {
        return $this->pageSize * $this->page;
    }
}
